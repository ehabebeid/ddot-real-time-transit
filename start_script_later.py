#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        Run Script Later
# Author:      Ehab Ebeid
# Created:     2017/06/29
# -------------------------------------------------------------------------------
from __future__ import division, print_function
import datetime, time
from ddot_realtime import save_vehicle_locs

def main():
    d = raw_input("What day would you like to run the save_vehicle_locs script (YYYY-MM-DD)? ")
    
    year = int(d[0:4])
    month = int(d[5:7])
    day = int(d[8:])
    
    t = raw_input("What time would you like to run the script (HH:MM)? ")
    print(year, month, day)
    HH = int(t[:2])
    MM = int(t[3:])
    print(HH, ":", MM)
    
    then = datetime.datetime(year, month, day, HH, MM)
    now = datetime.datetime.now()
    diff = then - now
    seconds = diff.total_seconds()

    while seconds > 0:
        diff = then - datetime.datetime.now()
        seconds = diff.total_seconds()
        print(str(int(seconds))+' left.', end='\r'),
        time.sleep(1)
    
    csvpath = save_vehicle_locs('dc-streetcar', 'h_route', 21, 10, r"\\ddotfile02\DDOTSHARE\DDOTFSHARE\GIS\GTFS\Real-Time GTFS\Streetcar AVL")
    print('Results written to:')
    print('        ',csvpath)

    raw_input('Press ENTER to exit.')
    
if __name__ == '__main__':
    main()