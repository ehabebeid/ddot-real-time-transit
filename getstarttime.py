#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        DDOT Real-Time Transit
# Author:      Huixin Rao
# Created:     2017/11/13
# -------------------------------------------------------------------------------
from ddot_realtime import new_way_get_start_time
from utils import get_service_day
import datetime
import utils as u

########block time to get start time
nextbus_url = 'http://webservices.nextbus.com/service/publicJSONFeed?command=' #NextBus JSON feed link

agencydict = {
    'dc-circulator': ['yellow','green','blue','turquoise','orange','red'],
    'dc-streetcar' : ['red']
    }
route_id_to_NB_tag = { #Circulator route colours mapped onto NextBus Circulator route tags
    "dc-circulator":
        {
        "yellow": "yellow",
        "green": "green",
        "blue": "blue",
        "turquoise":"rosslyn",
        "orange": "potomac",
        "red": "mall"
        },
                    
    "dc-streetcar":
        {
        "red": "h_route"
        }
    }

serviceClass = {
    'dc-circulator': ['wkd','wkd','wkd','wkd','fri','sat','sun'],
    'dc-streetcar' : ['mtwth','mtwth','mtwth','mtwth','fri','sat','sun']
    }

NB_tag_to_route_id = {
    "yellow": "Yellow",
    "green": "Green",
    "blue": "Blue",
    "rosslyn":"Turquoise",
    "potomac": "Orange",
    "mall": "Red",
    "h_route": "Red"
    }

def get_schedule(agency,route,SC):
    url_schedule = nextbus_url + 'schedule&a=' + agency +'&r=' + route
    print(url_schedule)
    json_schedule = u.get_json_from_url(url_schedule)
    s={}
    for i in json_schedule.get('route'):
        if i.get('serviceClass')==SC:
            dirt = i.get('direction')
            schedule=i.get('tr')
            s[dirt]={}
            for se in schedule:
                block = se.get('blockID')
                start_time = se.get('stop')[0].get('content')
                if block not in s[dirt]:
                    s[dirt][block]=[start_time]
                else:
                    s[dirt][block].append(start_time)
    return s

      
def get_day_schedule_dict(agency):
    idx=datetime.datetime.today().weekday()
    s={}
    SC = serviceClass.get(agency)[idx]
    for _, route in route_id_to_NB_tag[agency].iteritems():
        route_id = NB_tag_to_route_id[route]
        s[route_id] = get_schedule(agency,route,SC)
    return s
                  


def find_block(route,schedule_dict,block,dirtag,tftup):
    
    time_list=schedule_dict[route][dirtag][block]
    start_time = binary_search(time_list)
    
    return  get_start_time_block(start_time,tftup)   

def binary_search(time_list):
    start=0
    last=len(time_list)
    found=False 
    now=datetime.datetime.now().time()
    while start<last and not found:
        mid=(start+last)//2

        if now<datetime.datetime.strptime(time_list[mid],"%H:%M:%S").time():
            last=mid
            if last-start==1:
                found=True
        else:
            start=mid
            if last-start==1:
                found=True
    return (time_list[start])

      
def get_start_time_block(start_time, tftup):
    """
    get the start time for the vehicle just set off from stop 0, which is the start time of current trip
    :param tftup: current trip info
    :return estimate start time in format hh:mm:ss
    """
    delta = u.string_to_dt(start_time) - u.string_to_dt(tftup.start_time)
    num_of_interval = int(delta.total_seconds() // tftup.headway_secs)
    start_time_trip = u.string_to_dt(tftup.start_time) + datetime.timedelta(0, num_of_interval * tftup.headway_secs)
    
    dt = datetime.time(hour = int(start_time_trip.hour),\
                       minute = int(start_time_trip.minute),
                       second = int(start_time_trip.second)).isoformat()

    return dt
 













#################block way to get start time


'''
def get_start_date(previous, myentity):
    if previous == 0 or myentity.id not in previous:
        start_date = get_service_day()
    elif previous[myentity.id].vehicle.trip.start_date != get_service_day():
        start_date = get_service_day()
    else:
        start_date = previous[myentity.id].vehicle.trip.start_date        
    return start_date


def get_start_time_circulator(agency,previous,myentity):
    # next day
    if start_date != previous[myentity.id].vehicle.trip.start_date:
        
    
    return start_date,start_time

def get_start_time_streetcar(agency,previous,myentity):


    return start_date,start_time

check_dict={}
def check_start_time():
    for id,entity in previous.iteritems():
        if not entity.trip_update.trip.trip_id in check_dict:
            check_dict[entity.trip_update.trip.trip_id]=[id]
        else:
            check_dict[entity.trip_update.trip.trip_id].append(id)
        start_time=[]
        for id in check_dict[entity.trip_update.trip.trip_id]:
            if previous[id].vehicle.trip.start_time not in start_time:
                start_time.append(previous[id].vehicle.trip.start_time)
            else:
                idx = start_time.index(previous[id].vehicle.trip.start_time)
                
                seq_id_same_start_time = previous[id[idx]].vehicle..current_stop_sequence
                seq_id = previous[id].vehicle..current_stop_sequence
                
   
####
if agency == 'dc-circulator':
                        if previous!=0 and myentity.id in previous:
                            if seq == 0:
                                start_time,new = new_way_get_start_time(agency, trip_id, tftup, previous[myentity.id].vehicle.position.latitude, previous[myentity.id].vehicle.position.longitude,myentity.vehicle.position.latitude,myentity.vehicle.position.longitude,previous[myentity.id].vehicle.trip.start_time)
                                if new == 1:
                                    print("cl Bon Voyage")
                                    print(datetime.datetime.now())
                                    myentity.vehicle.trip.start_time = start_time
                                    myentity.vehicle.trip.start_date = get_service_day()
                                    print(myentity.id,myentity.vehicle.trip.trip_id, myentity.vehicle.current_stop_sequence, myentity.vehicle.trip.start_time,"pre:",previous[myentity.id].vehicle.current_stop_sequence,previous[myentity.id].vehicle.trip.start_time)
                                        
                                else:
                                    myentity.vehicle.trip.start_time = previous[myentity.id].vehicle.trip.start_time
                                    myentity.vehicle.trip.start_date = previous[myentity.id].vehicle.trip.start_date
                                
                            else:
                                myentity.vehicle.trip.start_time = previous[myentity.id].vehicle.trip.start_time
                                myentity.vehicle.trip.start_date = previous[myentity.id].vehicle.trip.start_date
     
                        else:#first round
                            print(myentity.id,"cl estimate start time")
                            myentity.vehicle.trip.start_time = estimate_start_time(myentity.vehicle.current_stop_sequence,tftup)
                            diff2 = diff_time2(myentity.vehicle.trip.start_time, tftup.start_time)
                            if diff2 < 0:
                                myentity.vehicle.trip.start_time = tftup.start_time 
                                myentity.vehicle.trip.start_date = get_service_day()
                            print(myentity.vehicle.current_stop_sequence, myentity.vehicle.trip.start_time)
                        entity_tu.trip_update.trip.start_time = myentity.vehicle.trip.start_time
                        entity_tu.trip_update.trip_start_date = myentity.vehicle.trip.start_date
                        
                    if agency == 'dc-streetcar':
                        
                        if previous != 0 and myentity.id in previous:
                            if trip_id != previous[myentity.id].vehicle.trip.trip_id:
                                print("sc Bon Voyage")
                                print(datetime.datetime.now())
                                myentity.vehicle.trip.start_time = get_start_time5(tftup)
                                myentity.vehicle.trip.start_date = get_service_day()
                                print(myentity.id,myentity.vehicle.trip.trip_id, myentity.vehicle.current_stop_sequence, myentity.vehicle.trip.start_time,"pre:",previous[myentity.id].vehicle.trip.trip_id,previous[myentity.id].vehicle.current_stop_sequence,previous[myentity.id].vehicle.trip.start_time)
                                
                            else:
                                myentity.vehicle.trip.start_time = previous[myentity.id].vehicle.trip.start_time
                                myentity.vehicle.trip.start_date = previous[myentity.id].vehicle.trip.start_date
                        else:
                            print(myentity.id,"sc estimate start time")
                            myentity.vehicle.trip.start_time = estimate_start_time(myentity.vehicle.current_stop_sequence,tftup)
                            diff2 = diff_time2(myentity.vehicle.trip.start_time, tftup.start_time)
                            if diff2 < 0:
                                myentity.vehicle.trip.start_time = tftup.start_time 
                                myentity.vehicle.trip.start_date = get_service_day()
                            print(myentity.vehicle.current_stop_sequence, myentity.vehicle.trip.start_time)
                        
                        entity_tu.trip_update.trip.start_time = myentity.vehicle.trip.start_time
                        entity_tu.trip_update.trip_start_date = myentity.vehicle.trip.start_date
####


t1=time.time()
get_service_day()
t2=time.time()
print(t2-t1)

t4=time.time()
if 20171112!=get_service_day():
    dd=get_service_day()
else:
    dd=20171112
t3=time.time()
print(t3-t4)
'''