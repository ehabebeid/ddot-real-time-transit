#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        Streetcar XY for as long as specified
# Author:      Ehab Ebeid
# Created:     2017/06/28
# -------------------------------------------------------------------------------

from __future__ import division
import time, datetime
from ddot_realtime import save_vehicle_locs

def main():
    agency = 'dc-streetcar'
    route = 'h_route'
    inittime = time.time()

    # User inputs duration in hours
    dh = float(raw_input('I would like to run the script for ... hours. (1 - 24 hours, decimals accepted) '))
    if dh <= 0 or dh > 24:
        raise ValueError('Hours must be between 1 and 24. Run again with a valid hour value.') 

    ps = int(raw_input('I would like to read the NextBus data every ... seconds. '))
    if ps == 0:
        raise ValueError('Period cannot be 0.')
    elif ps >= dh*60*30:
        raise ValueError('Period cannot be more than half the duration time.')

    csvloc = r"\\ddotfile02\DDOTSHARE\DDOTFSHARE\GIS\GTFS\Real-Time GTFS\Streetcar AVL"

    csvpath = save_vehicle_locs(agency, route, dh, ps, csvloc)

    termtime = time.time()
    print 'Time elapsed: '+str(int(termtime-inittime))+' seconds. Terminated at '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"."
    print 'Results written to:'
    print '        ',csvpath
    print
    raw_input('Press ENTER to exit.')

if __name__ == '__main__':
    main()