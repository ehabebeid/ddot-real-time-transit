#!/usr/bin/env python2.7.3
from __future__ import division
import os, sys, time, shutil, datetime
import google.transit.gtfs_realtime_pb2 as g  # @UnresolvedImport
from ddot_realtime import get_nextbus_agency_vl, current_stop_sequence2, \
        get_trip_id, get_prediction, estimate_start_time, diff_time2,\
        new_way_get_start_time, get_start_time5
from utils import string_to_dt,get_service_day
import logging
from logging.handlers import TimedRotatingFileHandler
from alert_writer import agencies

from dask.dataframe.tests.test_rolling import idx

from ddot_realtime import get_seq_dict
from stop_dict_generator import get_stop_dict
import utils as u
from getstarttime import binary_search,get_start_time_block
from collections import namedtuple

scriptloc = sys.path[0]+'\\RT Feeds'
ext = '.pb'#'.txt'#

agencies = ['dc-circulator','dc-streetcar']
webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

route_id_to_NB_tag = { #Circulator route colours mapped onto NextBus Circulator route tags
    "dc-circulator":
        {
        "yellow": "yellow",
        "green": "green",
        "blue": "blue",
        "turquoise":"rosslyn",
        "orange": "potomac",
        "red": "mall"
        },
                    
    "dc-streetcar":
        {
        "red": "h_route"
        }
    }

NB_tag_to_route_id = {
    "yellow": "Yellow",
    "green": "Green",
    "blue": "Blue",
    "rosslyn":"Turquoise",
    "potomac": "Orange",
    "mall": "Red",
    "h_route": "Red"
    }
agencydict = {
    'dc-circulator': ['yellow','green','blue','turquoise','orange','red'],
    'dc-streetcar' : ['red']
    }
dir_id_dict = {
    "dc-circulator": 
        {
        'blues': 0,
        'bluen': 1,
        'turquoisee': 0,
        'turquoisew': 1,
        'yelloww': 0,
        'yellowe': 1,
        'greenn': 1,
        'greens': 0,
        'orangew': 0,
        'orangee': 1,
        'redw': 0,
        'rede': 1
            },
    "dc-streetcar":
        {
        'redw': 1,
        'rede': 0
            }
    }

serviceClass = {
    'dc-circulator': ['wkd','wkd','wkd','wkd','fri','sat','sun'],
    'dc-streetcar' : ['mtwth','mtwth','mtwth','mtwth','f','sat','sun']
    }


list_id_seq = get_seq_dict(agency)

stopdic_loc = {}
stopdic_tag = {}
for agency in agencydict.keys():
    #stop_dic[agency]=get_stop_dict(agency)
    stopdic_loc[agency], stopdic_tag[agency] = get_stop_dict(agency)

nextbus_url = 'http://webservices.nextbus.com/service/publicJSONFeed?command=' 



def get_schedule():
    schedule={}
    for a in route_id_to_NB_tag:
        schedule[a]={}
        for b in route_id_to_NB_tag[a]:
            schedule[a][route_id_to_NB_tag[a][b]]={}
            url_schedule = nextbus_url + 'schedule&a=' + a +'&r=' + route_id_to_NB_tag[a][b]
            json_schedule = u.get_json_from_url(url_schedule).get('route')
            for i in json_schedule:
                if i['serviceClass'] not in schedule[a][route_id_to_NB_tag[a][b]]:
                    schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']]={}
                schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']][i['direction']]={}
                for k in i.get('tr'):
                    if k.get('blockID') not in schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']][i['direction']]:
                        schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']][i['direction']][k.get('blockID')]=[k.get('stop')[0].get('content')]
                    else:
                        schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']][i['direction']][k.get('blockID')].append(k.get('stop')[0].get('content'))
    return schedule      

schedule=get_schedule() #key:agency, route, serviceClass,dirTag,block

TUInfo = namedtuple('TUInfo', 'trip_id, route_id, direction_id, start_time, start_date, vehicle_id, stop_seq, stop_id, arrival')

def get_prediction_each_stop(tripsdict):
    
    filename = filename_tu = agency+'-'+'tripupdates'+ext
    fm = g.FeedMessage()
    
    ## Feed Header
    fm.header.gtfs_realtime_version = '2.0'
    fm.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number #enum: full dataset
    count = 0

    for a in route_id_to_NB_tag:
        print a 
        for b in route_id_to_NB_tag[a]:
            #print b,route_id_to_NB_tag[a][b]
            taglist = stopdic_tag.get(a).get(route_id_to_NB_tag[a][b])
            route_id = NB_tag_to_route_id.get(route_id_to_NB_tag[a][b])
            route_id='yellow'
            #print(taglist)
            for id,tag in taglist.iteritems():
                url = nextbus_url + 'predictions&a=' + a +'&r=' + route_id + '&s=' + tag
                
                #print url
                try:
                    pre_dict = u.get_json_from_url(url).get('predictions')
                except:
                    print "this stop no prediction"
                    pre_dict=[]
                    pass
                if 'direction' in pre_dict:
                    #    
                    #route_id = NB_tag_to_route_id.get(route_id_to_NB_tag[a][b])
                    for p in pre_dict.get('direction').get('prediction'):
                        print p.vehicle
                    stop_id = id
                    vehicle = pre_dict.get('direction').get('prediction')[0]
                    vehicle_id = vehicle.get('vehicle')
                    arrival_time = int(vehicle.get('epochTime'))/1000
                    dirTag = vehicle.get('dirTag')
                    direction_id = dir_id_dict[a][b+dirTag[0]]
                    block = vehicle.get('block')
                    #    
                    trip_id, tftup = get_trip_id(tripsdict[a], route_id, direction_id)
                    
                    if int(id) in list_id_seq.get(a).get(int(trip_id)):
                        myentity = fm.entity.add()
                        myentity.id = trip_id+vehicle_id+id
                        stop_sequence = list_id_seq.get(a).get(int(trip_id)).index(int(id))
                        idx=datetime.datetime.today().weekday()
                        SC = serviceClass.get(a)[idx]
                        #    
                        start_time_list_per_block = schedule.get(a).get(route_id_to_NB_tag[a][b]).get(SC).get(dirTag).get(block)
                        start_time = binary_search(start_time_list_per_block)
                        start_time = get_start_time_block(start_time,tftup) 
                        start_date = str(get_service_day())
                    #   
                    #print(trip_id) 
                    #print(list_id_seq.get(a).get(int(trip_id)))
                    
                        
                        
                        myentity = fm.entity.add()
                        
                        tu= TUInfo(trip_id, route_id, direction_id, start_time, start_date, vehicle_id, stop_sequence, stop_id, arrival_time)
                        
                        
                        
                        
    return tu
'''
block_dict={}
def XXX(agency):



  
for agency in agencies:    
    vehiclelist = get_nextbus_agency_vl(agency)
    for vehicle in vehiclelist:    
        print "#",vehicle.vehicle_id
        if vehicle.route_id:
            r = route_id_to_NB_tag[agency][vehicle.route_id.lower()]
            #taglist = stopdic_tag.get(agency).get(r)
            if vehicle.direction_id is not None:
                trip_id, tftup = get_trip_id(tripsdict[agency], vehicle.route_id, vehicle.direction_id)
                seq,current_stop_id,not_near_stop= current_stop_sequence2(agency, trip_id, vehicle.lat, vehicle.lon)
                if not_near_stop==0:
                    #print("route:",r, trip_id)
                    taglist = stopdic_tag.get(agency).get(r)
                    #
                    has=0
                    #tag=taglist[int(current_stop_id)]
                    list_id_seq = get_seq_dict(agency).get(int(trip_id))
                    if int(current_stop_id) in list_id_seq:
                        idx = list_id_seq.index(int(current_stop_id))
                    else:
                        idx = 0   
                    while has==0 and idx<len(list_id_seq):
                        #print("idx",idx,len(list_id_seq))
                        stop_id = list_id_seq[idx]
                        #print(int(stop_id), idx, list_id_seq)
                        if int(stop_id) in taglist:
                            tag = taglist.get(int(stop_id))            
                            url = nextbus_url + 'predictions&a=' + agency +'&r=' + r + '&s=' + tag
                            try:
                                pre_dict = u.get_json_from_url(url).get('predictions')
                            except:
                                print "this stop no prediction"
                                pre_dict=[]
                                pass
                            if 'direction' in pre_dict:
                                for p in pre_dict.get('direction').get('prediction')[0:2]:
                                    #print p.get('vehicle')
                                    if int(p.get('vehicle'))==int(vehicle.vehicle_id):
                                        #print("stopid:",stop_id)
                                        #print("route_id",r)
                                        #print("trip_id:",trip_id)
                                        #print p
                                        #print(start_time(agency,r,p.get('dirTag'),p.get('block'),tftup))
                                        get_in_feed(fm,trip_id,vehicle,agency,r,p,idx,stop_id)
                                        #print()
                                        has=1
                                        break
                                    else:
                                        idx+=1
                            else:
                                print "no prediction"
                        else:
                            idx+=1
                else:
                    print "not_near_stop"
            else:
                print "!no direction",vehicle
        else:
            print "no route id"                        

'''                          

def get_in_feed(fm,trip_id,tftup,vehicle,agency,r,p,idx,stop_id):
    myentity = fm.entity.add()
    myentity.id = vehicle.vehicle_id+'_'+vehicle.route_id+'_'+trip_id
    myentity.trip_update.timestamp = int(time.time())
    myentity.trip_update.trip.trip_id=trip_id
    myentity.trip_update.trip.route_id=vehicle.route_id
    myentity.trip_update.trip.direction_id=vehicle.direction_id
    myentity.trip_update.trip.start_time=start_time(agency,r,p.get('dirTag'),p.get('block'),tftup)
    myentity.trip_update.trip.start_date=str(get_service_day())
    #myentity.trip_update.trip.schedule_relation
    myentity.trip_update.vehicle.id=myentity.trip_update.vehicle.label=vehicle.vehicle_id
    #myentity.trip_update.stop_time_update
    stu=myentity.trip_update.stop_time_update.add()
    stu.stop_sequence=idx
    stu.stop_id=str(stop_id)
    stu_arrival=int(p.get('epochTime'))/1000
    #stu.schedule_relationship=
    print myentity
                                             

                        
def start_time(a,r,dirTag,block,tftup):                       
    idx=datetime.datetime.today().weekday()
    SC = serviceClass.get(a)[idx]
    print(a,r,SC,dirTag,block)
    start_time_list_per_block = schedule.get(a).get(r).get(SC).get(dirTag).get(block)
    start_time = binary_search(start_time_list_per_block)
    start_time = get_start_time_block(start_time,tftup) 
    return start_time


'''                  
                    
                    idx = get_seq_dict(agency).get(int(trip_id)).index(int(current_stop_id))
                    next_stop_id=get_seq_dict(agency).get(int(trip_id))[idx+1]
                    
                     
                     
                    list_id_seq = get_seq_dict(agency).get(int(trip_id))
                    idx=list_id_seq.index(int(current_stop_id))
                    for id in list_id_seq[idx:len(list_id_seq)]:
                        if id in taglist:
                            print trip_id, id, taglist[id]
                            url = nextbus_url + 'predictions&a=' + agency +'&r=' + r + '&s=' + taglist[id]
                            try:
                                pre_dict = u.get_json_from_url(url).get('predictions')
                            except:
                                print "this stop no prediction"
                                pre_dict=[]
                                pass
                            
                            if 'direction' in pre_dict:
                                #    
                                #route_id = NB_tag_to_route_id.get(route_id_to_NB_tag[a][b])
                                for p in pre_dict.get('direction').get('prediction'):
                                    if p.get('vehicle')==vehicle.vehicle_id:
                                        print p
                                        break
            else:
                print "no direction"
        else:
            print "no route id"
                            
             seq,current_stop_id,not_near_stop= current_stop_sequence2(agency, trip_id, vehicle.lat, vehicle.lon)
             prediction_list = get_prediction(agency,vehicle.route_id,current_stop_id, vehicle.direction_id)
             if prediction_list:
                 for p in prediction_list: 
                     print p.get('vehicle')
        
        
        
time1=time.time()
ss=get_prediction_each_stop(agency,tripsdict)
time2=time.time()
time2-time1
        
'''    
              
            
    








'''

def write_trip_updatea(agency):
    filename = filename_tu = agency+'-'+'tripupdates'+ext
    
    vehiclelist = get_nextbus_agency_vl(agency)
    
    fm = g.FeedMessage()
    
    ## Feed Header
    fm.header.gtfs_realtime_version = '2.0'
    fm.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number #enum: full dataset
    count = 0
    
    if vehiclelist:
        for vehicle in vehiclelist:
            
            
            
    sp_dict={}
    for vehicle in vehiclelist:
        sp_dict[vehicle.vehicle_id]=vehicle.speed
    return sp_dict
'''