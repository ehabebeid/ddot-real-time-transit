#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        use bishop api as input for trip update to google
# Author:      huixin rao
# Created:     2018/01/24
# -------------------------------------------------------------------------------

import os, sys, time, shutil
import google.transit.gtfs_realtime_pb2 as g
from get_peak import get_vehicle, get_gtfsid, get_trip_id2, get_trip_info, get_eta, get_seq, find_trip_direction, get_direction
from utils import get_service_day
import logging
from logging.handlers import TimedRotatingFileHandler


scriptloc = sys.path[0]+'\\RT Feeds'
ext = '.pb'#'.txt'#

agency = 'dc-circulator'
webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

routes_dict= {11318: "Yellow",#u'GT-US',
              11319: "Green",#u'WP-AM',
              11320: "Blue",#u'US-NY',
              11321: "Turquoise",#u'RS-DP',
              11322: "Orange",#u'PS',
              11323: "Red"}#"u'NM'}

####logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
path = os.getcwd()
#logging handler
fh = TimedRotatingFileHandler(path+'/temp/bishoptest.log', when = 'h',interval = 1)
formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
######

returnlist={}

def write_trip_update_bishop(today_trip, previous):
    '''
    write protobug entity from vehicle list from bishop peak source
    :param today_trip : list of trips operated today, key is the trip id, value is the name tuple
    '''
    
    filename = 'dc-circulator-'+'tripupdates'+ext
    
    vehiclelist = get_vehicle()
    count = 0    
    # Feed Message
    fm = g.FeedMessage()
    
    ## Feed Header
    fm.header.gtfs_realtime_version = '2.0'
    fm.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number #enum: full dataset
    returnlist={}
    for vehicle in vehiclelist:
        if vehicle.tripID!=0 and get_eta(vehicle.route_id, vehicle.nextStopID)  and get_gtfsid(vehicle.nextStopID):
            myentity = fm.entity.add()
            
            myentity.id = vehicle.vehicle_id + "_" \
            + routes_dict[vehicle.route_id]  + "_" \
            + str(vehicle.tripID)
            print(vehicle)
            
            #logger.info(myentity)
            
            myentity.trip_update.timestamp =  vehicle.timestamp
            myentity.trip_update.vehicle.id = myentity.trip_update.vehicle.label = vehicle.vehicle_id
            myentity.trip_update.trip.route_id = routes_dict[vehicle.route_id]
            myentity.trip_update.trip.schedule_relationship = 0
            
            stu = myentity.trip_update.stop_time_update.add()
            stu.stop_id = get_gtfsid(vehicle.nextStopID )
            stu.arrival.delay = vehicle.minsLate*60
            stu.arrival.time = get_eta(vehicle.route_id, vehicle.nextStopID)
             
            start_time = get_trip_info(vehicle.tripID,vehicle.nextStopID)
            myentity.trip_update.trip.start_time = start_time
            myentity.trip_update.trip.start_date = str(get_service_day())
               
            if not previous or vehicle.vehicle_id not in returnlist.keys():#first recursion
                direction = get_direction(int(stu.stop_id), vehicle.route_id)
                if direction!=0:
                    trip_id, direction_id = get_trip_id2(today_trip, routes_dict[vehicle.route_id], direction)
                    print(trip_id,stu.stop_id)
                    if get_seq(stu.stop_id, trip_id):
                        stu.stop_sequence = get_seq(stu.stop_id, trip_id)
                    
                else:
                    trip_id, direction_id, seq = find_trip_direction(today_trip, routes_dict[vehicle.route_id], int(stu.stop_id))
                    stu.stop_sequence = seq
                myentity.trip_update.trip.trip_id = trip_id
                myentity.trip_update.trip.direction_id = direction_id
                
            elif previous[vehicle.vehicle_id][0] == vehicle.tripID:
                pre_entity = previous[vehicle.vehicle_id][1]

                myentity.trip_update.trip.trip_id = pre_entity.trip_update.trip.trip_id
                myentity.trip_update.trip.start_time = pre_entity.trip_update.trip.start_time
                myentity.trip_update.trip.start_date = pre_entity.trip_update.trip.start_date
                myentity.trip_update.trip.direction_id = pre_entity.trip_update.trip.direction_id
                if get_seq(stu.stop_id, myentity.trip_update.trip.trip_id):
                    stu.stop_sequence = get_seq(stu.stop_id, myentity.trip_update.trip.trip_id)
                
            else:
                direction = get_direction(int(stu.stop_id), vehicle.route_id)
                if direction!=0:
                    trip_id, direction_id = get_trip_id2(today_trip, routes_dict[vehicle.route_id], direction)
                    print(trip_id,stu.stop_id)
                    if get_seq(stu.stop_id, trip_id):
                        stu.stop_sequence = get_seq(stu.stop_id, trip_id)
                    
                else:
                    trip_id, direction_id, seq = find_trip_direction(today_trip, routes_dict[vehicle.route_id], int(stu.stop_id))
                    stu.stop_sequence = seq
                myentity.trip_update.trip.trip_id = trip_id
                myentity.trip_update.trip.direction_id = direction_id
                   
            
            
            #code for one stop for now, improve with following stops late
                
            count+=1
            print myentity
            
            logst = myentity.id + " " + myentity.trip_update.trip.trip_id + " "+\
                    myentity.trip_update.trip.start_time +" " + myentity.trip_update.trip.start_date+ " " +\
                    stu.stop_id 
            logger.info(logst)
        
            
            returnlist[vehicle.vehicle_id]=[vehicle.tripID, myentity]    
        else: print(vehicle)
        
    '''
    eta_list=get_eta()
    
    for record in eta_list:
        tuentity = fm.entity.add()
        tuentity.id = record.get("routeID") + "_" + record.get("stopID")
        tuentity.trip_update.trip.route_id = record.get("routeID")
        gtfs_id, direction = get_gtfsid(record.get("stopID"))
        tuentity.trip_update.stop_time_update.stop_id = gtfs_id
        trip_id, direction_id = get_trip_id2(routes_dict[record.get("routeID")], direction)
        start_time = get_trip_info(vehicle.tripID)    
        no start time!!!!! 
    '''      
        
        
    fm.header.timestamp = int(time.time())
    print '    Wrote', count, 'vehicles pos and trip update updated with complete AVL info of dc circulator.'
    
    st='    Wrote '+str(count)+' vehicles pos and trip update updated with complete AVL info of '+str(agency)
    logger.info(st)
    logger.info(' ')
    
    f = open(scriptloc+'\\'+filename, "wb")
    f.write(fm.SerializeToString())
    f.close()
    
    newpath = webloc+filename
    shutil.copyfile(os.path.join(scriptloc, filename), newpath)
    
    return returnlist
#time1 = time.time()
#write_trip_update_bishop()
#time2 = time.time()
#print(time2-time1)


    '''        
            if vehicle.vehicle_id in returnlist.keys():
                if previous[vehicle.vehicle_id][0] == vehicle.tripID:#same trip
                    pre_entity = previous[vehicle.vehicle_id][1]

                    myentity.trip_update.trip.trip_id = pre_entity.trip_update.trip.trip_id
                    myentity.trip_update.trip.start_time = pre_entity.trip_update.trip.start_time
                    myentity.trip_update.trip.start_date = pre_entity.trip_update.trip.start_date
                    myentity.trip_update.trip.direction_id = pre_entity.trip_update.trip.direction_id
                    if get_seq(stu.stop_id, myentity.trip_update.trip.trip_id):
                    stu.stop_sequence = get_seq(stu.stop_id, myentity.trip_update.trip.trip_id)
            
                else:#trip changes
            else:#no previous trip
     '''    