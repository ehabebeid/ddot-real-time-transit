#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        DDOT Real-Time Transit
# Author:      Huixin Rao
# Created:     2017/10/13
# -------------------------------------------------------------------------------
from __future__ import division

from math import sin, cos, sqrt, atan2, radians
from stop_dict_generator import get_stop_dict
from ddot_realtime import get_gtfs_df


webloc = '\\\\ddotwebapp03\\CirculatorData\\gtfs'
agencydict = {
    'dc-circulator': ['yellow','green','blue','turquoise','orange','red'],
    'dc-streetcar' : ['red']
    }

R = 6373.0
####get dictionary for stop since unchanged, just need to run one time

#todo: after change the stop_dict_generator to both streecar and circulator this can be used
stopdic_loc={}
stopdic_tag={}
for agency in agencydict.keys():
    #stop_dic[agency]=get_stop_dict(agency)
    stopdic_loc[agency], stopdic_tag[agency] = get_stop_dict(agency)
    














####from test2 for calculating the seq of vehicle

def current_stop_sequence(agency, route, trip_id, direction, lat, lon):
    #this is a list containing all the lonlat of the stops in the route+dir
    #route, trip_id, dir, can get from trips_dict
    """
    input agency and trip info and current location of the vehicle
    output the sequence of the closest stop to the vehicle
    """
    dic_of_stop_loc = stopdic_loc[agency]
    #df_of_stop_loc2=stop_dic[agency].get(route).get(1-direction)
    #print(df_of_stop_loc)
    trip_id=int(trip_id)
    list_id_seq = get_seq_dict(agency).get(trip_id)
   
    return get_seq2(dic_of_stop_loc, list_id_seq, lat, lon)

def distance(slat,slon,lat,lon):
    """
    calculate distance from two coordianate points
    input: lat of point1, longtitue of point1, lat of point2, longtitude of point2
    output: distance
    """ 
    lat1=radians(slat)
    lon1=radians(slon)
    lat2=radians(lat)
    lon2=radians(lon)
    dlon=lon2-lon1
    dlat=lat2-lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    #c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R *  2 * atan2(sqrt(a), sqrt(1 - a))
    return distance

def get_seq2(dic_stop, list_seq, lat, lon):
    temp=-1
    not_near_stop=0
    for i in range(len(list_seq)):
        stop_id=int(list_seq[i])
        if stop_id in dic_stop:
            s_lat,s_lon=dic_stop[stop_id][0],dic_stop[stop_id][1]
            dis = distance(s_lat,s_lon,lat,lon)
            if temp>=0 and dis>=temp:
                break
            else:
                temp=dis
    if dis>1:
        not_near_stop=1
    return (i-1),str(list_seq[i-1]),not_near_stop


def get_seq_dict(agency):
    stopseq=get_gtfs_df(webloc, agency, 'stop_times')
    dict_seq={}
    for trip in set(stopseq['trip_id']):
        dict_seq[trip] = stopseq.loc[stopseq['trip_id']==trip,'stop_id'].tolist()
    return dict_seq



####binary search



### iterate all for the closest
def current_stop_sequence2(agency, route, trip_id, direction, lat, lon):
    #this is a list containing all the lonlat of the stops in the route+dir
    #route, trip_id, dir, can get from trips_dict
    """
    input agency and trip info and current location of the vehicle
    output the sequence of the closest stop to the vehicle
    """
    dic_of_stop_loc = stopdic_loc[agency]
    #df_of_stop_loc2=stop_dic[agency].get(route).get(1-direction)
    #print(df_of_stop_loc)
    trip_id=int(trip_id)
    list_id_seq = get_seq_dict(agency).get(trip_id)
   
    return get_closest_stop(dic_of_stop_loc, list_id_seq, lat, lon)


def get_closest_stop(dic_stop, list_seq, lat, lon):
    temp=100
    temp_index=-1
    not_near_stop=0
    for i in range(len(list_seq)):
        if int(list_seq[i]) in dic_stop:
            s_lat,s_lon=dic_stop[int(list_seq[i])][0],dic_stop[int(list_seq[i])][1]
            dis = distance(s_lat,s_lon,lat,lon)
            if dis<temp:
                temp=dis
                temp_index=i
    if temp>1:
        not_near_stop=1
    return temp_index,str(list_seq[temp_index]),not_near_stop

'''        
####from test2 for calculating the seq of vehicle

def current_stop_sequence2(agency, route, trip_id, direction, lat, lon):
    #this is a list containing all the lonlat of the stops in the route+dir
    #route, trip_id, dir, can get from trips_dict
    """
    input agency and trip info and current location of the vehicle
    output the sequence of the closest stop to the vehicle
    """
    dic_of_stop_loc = stopdic_loc[agency]
    #df_of_stop_loc2=stop_dic[agency].get(route).get(1-direction)
    #print(df_of_stop_loc)
    trip_id=int(trip_id)
    df_id_seq = get_seq_dict2(agency).get(trip_id)

    #print(df_id_seq)
    seq,stop_id,not_near=get_seq(dic_of_stop_loc, df_id_seq, lat, lon)
    
    return seq,stop_id,not_near


def get_seq(dic_stop, df_seq, lat, lon):
    """
    input: dataframe of stops containing information: ['stop_id', 's_lat',  's_lon', 'direction', 'stoptitle', 'detail', 'route', 'gtfs_id','tag']
            dataframe of seq containing information: [stop_id, stop_seq]
            lat,lon of the vehicle
    output: sequence of stop which is closest to the vehicle(int), and the name of the stop(string)
    """
    not_near_stop=0
    temp=-1
    seq=0
    #stop_id_current=df_seq
    for _,row in df_seq.iterrows():
        stop_id=row[0]
        stop_seq=row[1]
        #print(stop_id)
        if int(stop_id) in dic_stop:
            stop_lat_lon=dic_stop[int(stop_id)]
         #   print(stop_lat_lon)
        s_lat=stop_lat_lon[0]
        s_lon=stop_lat_lon[1]
        disnow=distance(s_lat,s_lon,lat,lon)
        if temp>=0 and disnow>=temp:
            seq=stop_seq-1
            break
        else:
            temp=disnow
    if disnow > 1:
        not_near_stop=1
    stop_id_current=df_seq.loc[df_seq['stop_sequence']==seq,'stop_id'].values[0]
    #stop_detail=df_stop.loc[df_stop["gtfs_id"]==stop_id_current,'detail'].values[0]
    return seq,str(stop_id_current),not_near_stop

def get_seq_dict2(agency):
    """
    read stop_times.txt return dictionary using trip id as key 
    and value is a dataframe containing stopid and stop sequence
    """
    dict_seq={}
    stopseq=get_gtfs_df(webloc, agency, 'stop_times')
    
    for trip in set(stopseq['trip_id']):
        dict_seq[trip]=stopseq.loc[stopseq['trip_id']==trip,['stop_id','stop_sequence']]
        #stopcsv.loc[stopcsv['route']==route].loc[stopcsv['direction']==direc]
    return dict_seq



agency='dc-circulator'
route='blue'
trip_id=10
direction=1
lat=38.8815043   
lon=-76.99503


s=time.time()
current_stop_sequence(agency, route, trip_id, direction, lat, lon)
e=time.time()
print(e-s)

s2=time.time()
current_stop_sequence2(agency, route, trip_id, direction, lat, lon)
e2=time.time()
print(e2-s2)
'''