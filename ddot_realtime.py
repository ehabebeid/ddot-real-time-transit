#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        DDOT Real-Time Transit
# Author:      Ehab Ebeid
# Created:     2017/06/21
# -------------------------------------------------------------------------------
from __future__ import division

import time
import utils as u
import pandas as pd

from pandas import DataFrame
from zipfile import ZipFile
from collections import namedtuple
from stop_dict_generator import get_stop_dict
from utils import get_service_day
import datetime
from math import sin, cos, sqrt, atan2, radians


import logging
import os
from logging.handlers import TimedRotatingFileHandler

logger=logging.getLogger('writer.test_tripupdate_vehicle.ddot_realtime')
logger.setLevel(logging.INFO)
path = os.getcwd()
fh=TimedRotatingFileHandler(path+'/temp2/test.log',
                                    when='h',interval=1)

formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

R = 6373.0
#### Named Tuples ####
CalException = namedtuple('CalendarException','service_id exception_type')
Service = namedtuple('CalendarService', 'monday tuesday wednesday thursday friday saturday sunday start_date end_date')
Freq = namedtuple('Freq', 'start_time end_time headway_secs')
Trip = namedtuple('Trip', 'route_id service_id direction_id')
TripFreq = namedtuple('TripFreq', 'route_id service_id direction_id start_time end_time headway_secs')

ServiceClass = namedtuple('NextBusServiceClass', 'monday tuesday wednesday thursday friday saturday sunday')

VehiclePos = namedtuple('VehiclePositionData', 'vehicle_id, route_id, direction_id, dirtag,  lat, lon, timestamp, bearing, speed, odometer')
VehicleInfo = namedtuple('VehicleInfo', 'vehicle_id, route_id, direction_id, lat, lon, timestamp, bearing, speed, odometer, trip_id, seq, start_time')
#### GLOBAL VARIABLES ####
agencydict = {
    'dc-circulator': ['yellow','green','blue','turquoise','orange','red'],
    'dc-streetcar' : ['red']
    }

webloc = '\\\\ddotwebapp03\\CirculatorData\\gtfs'

nextbus_url = 'http://webservices.nextbus.com/service/publicJSONFeed?command=' #NextBus JSON feed link

route_id_to_NB_tag = { #Circulator route colours mapped onto NextBus Circulator route tags
    "dc-circulator":
        {
        "yellow": "yellow",
        "green": "green",
        "blue": "blue",
        "turquoise":"rosslyn",
        "orange": "potomac",
        "red": "mall"
        },
                    
    "dc-streetcar":
        {
        "red": "h_route"
        }
    }

NB_tag_to_route_id = {
    "yellow": "Yellow",
    "green": "Green",
    "blue": "Blue",
    "rosslyn":"Turquoise",
    "potomac": "Orange",
    "mall": "Red",
    "h_route": "Red"
    }

route_id_to_code = {
    "dc-circulator":
        {
        "yellow": "GT-US",
        "green": "WP-AM",
        "blue": "US-NY",
        "turquoise":"RS-DP",
        "orange": "PS",
        "red": "NM"
        },
                    
    "dc-streetcar":
        {
            "red": "H-BNNG"
            }
    }

dir_id_dict = {
    "dc-circulator": 
        {
        'blues': 0,
        'bluen': 1,
        'turquoisee': 0,
        'turquoisew': 1,
        'yelloww': 0,
        'yellowe': 1,
        'greenn': 1,
        'greens': 0,
        'orangew': 0,
        'orangee': 1,
        'redw': 0,
        'rede': 1
            },
    "dc-streetcar":
        {
        'redw': 1,
        'rede': 0
            }
    }

serviceClass = {
    'dc-circulator': ['wkd','wkd','wkd','wkd','fri','sat','sun'],
    'dc-streetcar' : ['mtwth','mtwth','mtwth','mtwth','f','sat','sun']
    }

####get dictionary for stop since unchanged, just need to run one time
stopdic_loc = {}
stopdic_tag = {}
for agency in agencydict.keys():
    #stop_dic[agency]=get_stop_dict(agency)
    stopdic_loc[agency], stopdic_tag[agency] = get_stop_dict(agency)

#######get schedule dictionary, since unchanged, just need to run one time
def get_schedule():
    schedule={}
    for a in route_id_to_NB_tag:
        schedule[a]={}
        for b in route_id_to_NB_tag[a]:
            schedule[a][route_id_to_NB_tag[a][b]]={}
            url_schedule = nextbus_url + 'schedule&a=' + a +'&r=' + route_id_to_NB_tag[a][b]
            json_schedule = u.get_json_from_url(url_schedule).get('route')
            for i in json_schedule:
                if i['serviceClass'] not in schedule[a][route_id_to_NB_tag[a][b]]:
                    schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']]={}
                schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']][i['direction']]={}
                for k in i.get('tr'):
                    if k.get('blockID') not in schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']][i['direction']]:
                        schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']][i['direction']][k.get('blockID')]=[k.get('stop')[0].get('content')]
                    else:
                        schedule[a][route_id_to_NB_tag[a][b]][i['serviceClass']][i['direction']][k.get('blockID')].append(k.get('stop')[0].get('content'))
    return schedule      

schedule=get_schedule()

#### Classes ####
""" 
The classes and the function get_ddot_system are experimental,
for an alternative way to organise system data with real-time info.
"""
class DDOT_Transit:
    """
    Represents the entirety of DDOT's transit operations.
    """
    def __init__(self, services=None):
        self.services = [] if services is None else services
        
    def add_service(self, service):
        self.services.append(service)
    
    def __repr__(self):
        return "DDOT_Transit object."

class DDOT_Service:
    """
    Represents a transit service/'agency' offered by DDOT.
    """
    def __init__(self, name = None, agency_id = None, routes = None):
        
        self.routes = [] if routes is None else routes
            
        self.id = 'dc-'+name if agency_id is None else agency_id
        self.name = self.id[3:] if name is None else name
        
    def add_route(self, route):
        self.routes.append(route)
        
    def __repr__(self):
        return "DDOT_Service("+self.id+ ")"
    
class Route:
    """
    Represents a route on a DDOT transit service.
    """
    def __init__(self, code, nb_id, color, service, directions = None):
        self.code = code
        self.nb_id = nb_id
        self.color = color
        self.service = service
        
        self.directions = [] if directions is None else directions
    
    def add_dir(self, direction):
        self.directions.append(direction)
        
    def __repr__(self):
        return "Route("+self.service+", "+self.code+", "+self.color+ ")"

class Direction:
    """
    Represents a direction of a DDOT transit route..
    """
    def __init__(self, dir_id, cardinal_dir, route):
        self.dir_id = dir_id
        self.cardinal_dir = cardinal_dir
        self.route = route
        
    def __repr__(self):
        return "Direction("+self.route+", "+self.cardinal_dir+", "+str(self.dir_id)+ ")"

#### FUNCTIONS ####

###    ####
def get_ddot_system():
    """
    Returns a DDOT_Transit object of all DDOT services
    using the above dictionary.
    """
    ddot_system = DDOT_Transit()
    for agency, routes in agencydict.iteritems():
        service = DDOT_Service(agency_id=agency)
        for r in routes:
            code = route_id_to_code[service.id].get(r)
            nb_id = route_id_to_NB_tag.get(service.id).get(r)
            route = Route(code, nb_id, r, service.id)
            service.add_route(route)
            
            for routedir, dir_id in dir_id_dict.get(agency).iteritems():
                if routedir.startswith(r):
                    cardinal_dir = routedir[-1]
                    direction = Direction(dir_id, cardinal_dir, r)
                    route.add_dir(direction)
            
        ddot_system.add_service(service)
    return ddot_system

### General ###
def get_gtfs_df(webloc, archivename, filename):
    """
    Return a Pandas CSV DataFrame object of filename.txt,
    located within the .zip archive, with name
    archivename.zip located within webloc folder.
    
    :param webloc: str, location of GTFS .zip archive
    :param archivename: str, name of .zip archive, without '.zip'.
    :param filename: str, name of .txt file inside archive, without '.txt'. Must be one of the GTFS .txt files.
    :returns a pandas DateFrame object containing what's in the comma-separated txt/csv.
    """
    archive = ZipFile(webloc+"\\"+archivename+".zip")
    f = archive.open(filename+".txt")
    return pd.io.parsers.read_csv(f)  # @UndefinedVariable because PyDev can't read submodule

def str_to_tuple(string):
    """
    Make tuple out of string with underscore separated values.
    :param string: a value in string whose components are underscore-separated. e.g.: "
    :returns tuple: a tuple whose items are the components separated by underscores in string.
    """
    l = string.split("_")
    return tuple(l)

def get_trips_dict(agency):
    """
    Read the trips.txt file and return a dict where
    the key is the trip_id and the value is a tuple with
    some of the fields in the file, relating to route, service_id
    and direction.
    
    :param agency: a str identifying the transit agency. Assumes a agency.zip GTFS archive.
    :returns a dict of tuples, representing *all* trips for this transit agency's GTFS.
    """ 
    dicto = {}
    trips = get_gtfs_df(webloc, agency, 'trips')
    # trips.txt header row:
    # route_id,service_id,trip_id,trip_headsign,shape_id,direction_id,wheelchair_accessible,bikes_allowed
    for _, row in trips.iterrows():
        dicto[int(row['trip_id'])] = Trip(row['route_id'],row['service_id'],int(row['direction_id']))

    return dicto

def get_freq_dict(agency):
    """
    Read the frequencies.txt file and return a dict where the
    trip_id is the key and the value is a tuple of start and end time
    for that trip. Frequency is constant at 10 mins for Circulator.
    
    :param agency: a str identifying the transit agency. Assumes a agency.zip GTFS archive.
    :returns a dict of tuples, representing trip times/frequency information for *all* trips in this transit agency's GTFS.
    """ 
    freqs = {}
    freq = get_gtfs_df(webloc, agency, 'frequencies')
    # frequencies header:
    # trip_id,start_time,end_time,headway_secs,exact_times
    for _, row in freq.iterrows():
        freqs[row['trip_id']] = Freq(row['start_time'],row['end_time'],row['headway_secs'])
    return freqs

def get_services(agency, day=None, exp=True):
    """
    Read the calendar.txt file and return a dict where the key
    is the service_id and the value is a tuple with the rest of the
    fields. If today is provided, will filter them and return only
    those service_id's active today. If exp = True (default), accounts
    for service exceptions included in calendar_dates.txt
    
    :param agency: a str identifying the transit agency. Assumes a agency.zip GTFS archive.
    :param day: an 8-char str or int specifying a day. Optional.
    :param exp: bool, whether or not to account for CalException s active today
    :returns a dict of tuples, representing calendar information in the agency GTFS file.
             If today is provided then this dict includes only services active today.
    """
    dicto = {}
    cal = get_gtfs_df(webloc, agency, 'calendar')
    # calendar.txt header:
    # service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date
    if day is not None:
        date = u.string_to_date(day)
        dow = date.strftime("%A").lower() # lowercase name not number

    for _, row in cal.iterrows():
        startstring, endstring = row['start_date'], row['end_date']
        startdate, enddate = u.string_to_date(startstring), u.string_to_date(endstring)

        if day is None or\
           (row[dow]==1 and startdate <= date <= enddate): #
            dicto[row['service_id']] = Service(int(row['monday']),
                                               int(row['tuesday']),
                                               int(row['wednesday']),
                                               int(row['thursday']),
                                               int(row['friday']),
                                               int(row['saturday']),
                                               int(row['sunday']),
                                               startstring,
                                               endstring)
    # Start accounting for exceptions:
    if day is not None and exp is True:
        exceptions = get_exceptions(agency, day)        
        for x in exceptions:
            thisserviceid = x.service_id
            if x.exception_type == 1:
                # Service ID is activated today
                if thisserviceid not in dicto: # service ID should be added and is not currently in dict
                    servicetup = get_services(agency)[thisserviceid] # Write tuple by recursively calling function
                    dicto[x.service_id] = servicetup
        
            elif x.exception_type == 2:
                if thisserviceid in dicto: # Service has been disabled for today
                    del dicto[x.service_id] # remove it from dict
        # print 'Returning ', len(dicto), 'service types active for today.'

    return dicto

def get_exceptions(agency, day = None):
    """
    Read through exception dates for given agency, and 
    return (a dict of) list(s) of tuples. As in:
        Dict, where:
        Key: date as expressed in a string yyyymmdd
        Value: List(Tuple), each tuple representing an exception for Key date
        List Item/Tuple: (Service_ID, ExceptionType)
        Exception Type: 1 for adding service, 2 for removing service.
    
        List, of 'Value'.
    
    If today is not None and is an int date, 
    return only those exceptions active for today's date in a list, not a dictionary.
    
    Example returned list, for Christmas:
        [('Green_Monday_Thursday', 2),
        ('Orange_Winter_Monday_Friday', 2), 
        ('Yellow_Monday_Thursday', 2), 
        ('Blue_Winter_Monday_Friday', 2),
        ('Red_Winter_Monday_Friday', 2), 
        ('Turquoise_Monday_Thursday', 2)]
    
    :param agency: a str identifying the transit agency. Assumes a agency.zip GTFS archive.
    :param today: an int representing date, in the format yyyymmdd.
    :returns a list or a dict. If today is a date, then returns a list of exceptions active today.
             If today is None then returns a dict of *all* exceptions information in agency GTFS.
    """
    
    cexp = get_gtfs_df(webloc, agency, 'calendar_dates')
    dicto = {}
    l = [] # will be returned if today is passed
    
    for _, row in cexp.iterrows():
        
        t = CalException(row['service_id'], row['exception_type'])
        dateint = row['date']

        if day is None:
            if dicto.get(dateint) is None:
                    dicto[dateint] = [t]
            else:
                dicto[dateint].append(t)
            return dicto
        
        else:
            if day is not int: day = int(day)
            if dateint == day:
                l.append(t)
            return l

def get_day_trips(agency):
    """
    Returns a dict where key is trip id active today and value is a tuple
    concatenation of tuple dict value generated by get_freq_dict and get_trips_dict.
    Assumes route is active in both directions if scheduled as such.
    
    @attention: Call before and pass it as an argument to get_trip_id to save time.
    
    :param agency: a str identifying the transit agency. Assumes a agency.zip GTFS archive.
    :param day: an int representing date, in the format yyyymmdd.
    :returns a dict of tuples representing trips active today.
    """
    day = get_service_day()

    d = {}
    activeservice = get_services(agency, day, exp=True)
    freqs = get_freq_dict(agency)
    trips = get_trips_dict(agency)
    # freqs[row['trip_id']] = (row['start_time'],row['end_time'],row['headway_secs'])
    # trips[row['trip_id']] = (row['route_id'],row['service_id'],row['direction_id'],row['trip_headsign'])
    
    for trip_id, ttup in trips.iteritems():
        if ttup.service_id in activeservice:
            tup = TripFreq(*(ttup + freqs[trip_id]))
            d[trip_id] = tup
            #print tup
    
    # print 'Generated', len(d), 'trips on both directions.'
    return d
    
def get_trip_id(todaytripsdict, route_id, direction_id):
    """
    With a dict of the trips active today and through a series 
    of comparisons decide which trip the vehicle info belongs to.
    Returns trip_id matching GTFS file.
    
    :param todaytripsdict: a dict of today's trips for a given agency, in the format returned by get_day_trips(agency, today)
           This is used instead of generating it inside this so as to avoid generating for each vehicle.
    :param route_id: a str corresponding with the route_id field in GTFS/GTFS-RT feeds of this agency
    :param direction_id: an int corresponding with direction_id field in GTFS/GTFS-RT feeds of this agency
    :param datetime: experimental field, optional. Defaults to datetime obj now()
    :returns str trip_id, and corresponding tuple
    """
    for trip_id, tftup in todaytripsdict.iteritems():
        if route_id is not None and route_id == tftup.route_id:
            if direction_id is not None and direction_id == tftup.direction_id:
                start, end = u.string_to_dt(tftup.start_time), u.string_to_dt(tftup.end_time)
                if start <= datetime.datetime.now() <= end + datetime.timedelta(hours=1):
                    return str(trip_id), tftup



def estimate_start_time(seq,tftup):
    """
    estimate the start time for vehicles that does not have previous start time based on their current stop seq
    :param seq: sequence of stop that is closest to the current vehicle position
    :param tftup: current trip info
    :return estimate start time in format hh:mm:ss
    """
    time = datetime.datetime.now()
    seq = int(seq)
    start_time1 = time-datetime.timedelta(0,seq*tftup.headway_secs)
    delta = start_time1 - u.string_to_dt(tftup.start_time)
    num_of_interval = int(delta.total_seconds() // tftup.headway_secs)
    start_time_trip = u.string_to_dt(tftup.start_time) + datetime.timedelta(0,num_of_interval * tftup.headway_secs)
    
    dt = datetime.time(hour = int(start_time_trip.hour),\
                       minute = int(start_time_trip.minute),\
                       second = int(start_time_trip.second)).isoformat()
    
    return dt

def get_start_time5(tftup):
    """
    get the start time for the vehicle just set off from stop 0, which is the start time of current trip
    :param tftup: current trip info
    :return estimate start time in format hh:mm:ss
    """
    delta = datetime.datetime.now() - u.string_to_dt(tftup.start_time)
    num_of_interval = int(delta.total_seconds() // tftup.headway_secs)
    start_time_trip = u.string_to_dt(tftup.start_time) + datetime.timedelta(0, num_of_interval * tftup.headway_secs)
    
    dt = datetime.time(hour = int(start_time_trip.hour),\
                       minute = int(start_time_trip.minute),
                       second = int(start_time_trip.second)).isoformat()

    return dt

def diff_time(time_pre):
    """
    calculate the current time to the input
    :param time_pre: previous start time in format hh:mm:ss
    :return difference in minute
    """
    delta = datetime.datetime.now() - u.string_to_dt(time_pre)
                                                #int(datetime.datetime.today().date().strftime("%Y%m%d")))
    return delta.total_seconds()/60

def diff_time2(time1, time2):
    """
    calculate the delta between two time point in format hh:mm:ss
    :param time1,time2: previous start time in format hh:mm:ss
    :return difference in minute
    """
    FMT = '%H:%M:%S'
    tdelta = datetime.datetime.strptime(time1, FMT) - datetime.datetime.strptime(time2, FMT)
    return tdelta.total_seconds()/60
   
### NextBus-Specific Functions ###
def get_NB_schedule(agency, route):
    '''
    Returns a dict of NextBus schedule for given route and agency
    :param agency: a str NextBus identifier of a transit agency
    :param route: a str NextBus identifier of a transit route for given agency
    '''
    url = nextbus_url + 'schedule&a=' + agency +'&r=' + route
    return u.get_json_from_url(url)


def get_nextbus_vehicle_data(agency, vehicle):
    """
    For a given NextBus dict representing a single vehicle,
    return all values in types and units consumable by the GTFS-RT 
    Python Protobuf compiler. Return None if key not in dict.
    NextBus does not return odometer value but it is returned
    as None here for consistency.
    
    @attention: Recall that dict.get(key) returns None if key is not 
    found. You must test_tripudate for None, e.g.:
        if lat is not None: feedentity.vehicle.position.latitude = lat
        
    :param vehicle: a NB dict of vehicle location/info, can be obtained as get_nextbus_route_vl(agency, route, t).get('vehicles')
    :returns a VehiclePos named tuple of information ordered and converted in the formats accepted by GTFS-RT
    """
    
    vehicle_id = route_id = direction_id = dirtag = lat = lon = timestamp = bearing = speed = odometer = None
    if isinstance(vehicle, dict):
        vehicle_id = vehicle.get('id')

        route_id = NB_tag_to_route_id.get(vehicle.get('routeTag'))
        
        if 'dirTag' in vehicle:
            lookup = (route_id + vehicle.get('dirTag')[0]).lower() #route_id + first letter of NB dirTag (n,s,e,w), all lowercase
            direction_id = dir_id_dict.get(agency).get(lookup)
            dirtag = vehicle.get('dirTag')
            
        lat = float(vehicle.get('lat'))
        
        lon = float(vehicle.get('lon'))
        
        if 'secsSinceReport' in vehicle:
            timestamp = get_nextbus_timestamp(vehicle.get('secsSinceReport'))
        
        h = int(vehicle.get('heading'))
        bearing = h if h >=0 else None
        
        if vehicle.get('speedKmHr'): 
            speed = float(vehicle.get('speedKmHr'))*(5/18) # return speed strictly in m/s

    return VehiclePos(vehicle_id, route_id, direction_id, dirtag, lat, lon, timestamp, bearing, speed, odometer)

###get new vehicle info
def get_vehicle_info(agency, vehicle):
    """
    TODO: should chagne the vehiclePOS to this vehicleIfo (difference:have trip id, seq, start time)
    get vehicle information return as tuple
    :param agency: a str identifying the transit agency. Assumes a agency.zip GTFS archive
    :param vehicle:
    :return vehicle information tuple
    """
    t = get_nextbus_vehicle_data(agency, vehicle)
    
    todaytripsdict=get_day_trips(agency, get_service_day())
    trip_id, seq, start_time=999,999,'99:99:99'
    if (t.direction_id==0 or t.direction_id==1):
        
        trip_id, tftup = get_trip_id(todaytripsdict, t.route_id, t.direction_id)
    
        seq,stop_id,not_near=current_stop_sequence2(agency, t.route_id, trip_id, t.direction_id, t.lat, t.lon)
        start_time=tftup.start_time
    
    
    return VehicleInfo(t.vehicle_id, t.route_id, t.direction_id, t.lat, t.lon, t.timestamp, t.bearing, t.speed, t.odometer, trip_id, seq, start_time)

####

def get_nextbus_route_vl(agency, route, t=0):
    """
    Return a list of named tuples of AVL data for given route given the 
    desired agency id, NB route tag, and last time info was obtained.
    
    :param agency: a str NextBus identifier of a transit agency
    :param route: a str NextBus identifier of a transit route for given agency
    :param t: Last time this function was called, in *msec Posix time*. Optional, default 0.
    :returns a list of VehiclePos named tuples.
    """
    url = nextbus_url + 'vehicleLocations&a=' + agency + '&r=' + route + '&t=' + str(t)

    try:
        vehiclelist =  u.get_json_from_url(url).get('vehicle')
    except ValueError:
        vehiclelist = []
        
    vlist = []
    if vehiclelist:
        for vehicle in vehiclelist:
            t = get_nextbus_vehicle_data(agency, vehicle)

            vlist.append(t)

    return vlist

def get_nextbus_agency_vl(agency):
    alist = []
    for route in agencydict.get(agency):
        rlist = get_nextbus_route_vl(agency,route_id_to_NB_tag.get(agency).get(route))
        alist.extend(rlist)
        
    return alist

def get_nextbus_timestamp(secsSinceReport):
    """
    Convert the secsSinceReport value into epoch timestamp.
    :param secsSinceReport: The number of secs value since report
    :returns The timestamp field converted into Epoch time in seconds
    """
    return int(time.time())-int(secsSinceReport)

def get_nextbus_pred(agency, route, stop):
    """
    Return a dictionary of NextBus arrival predictions given  
    the desired agency id, NB route tag, and NB stop tag.
    :param agency: a str NextBus identifier of a transit agency
    :param route: a str NextBus identifier of a transit route for given agency
    :param stop: a str NextBus identifier of a stop/station on given route
    :returns a dict representation of the JSON feed, listing arrival predictions 
             for the given stop / vehicles on given route. 
    """
    url = nextbus_url + 'predictions&a=' + agency + '&r=' + route + '&s=' + stop
    return u.get_json_from_url(url)

def save_vehicle_locs(agency, route, dh, ps, csvloc):
    """
    Obtain real-time data using get_nextbus_route_vl for the given 
    agency and route, for the specified duration of dh hours,
    and at the frequency of once per ps seconds. 
    
    Print % progress along the way.
    
    Save a CSV file with lat/lon by timestamp and vehicleID,
    in the specified csvloc folder. Write the filename as
    'streetcarxy +[date and time of script termination]'.
    
    Return the full file path.
    :param agency: a str NextBus identifier of a transit agency
    :param route: a str NextBus identifier of a transit route for given agency
    :param dh: float, duration of script in hours
    :param ps: int, period of calling the API in seconds
    :param csvloc: str, location where script saves a .csv file.
    :returns a str for the full path for file saved, i.e. csvloc/filename.csv
    """
    # Initialise data frame
    columns = ['Epoch Time (s)', 'Vehicle ID','Timestamp (EST)', 'Route', 'Lat', 'Lon']
    df = DataFrame(columns=columns)
    
    hinsec = int(dh*60*60)
    
    # Loop by time, by vehicle
    for t in u.lrange(0,hinsec,ps):
        vehiclexylist = get_nextbus_route_vl(agency, route, 0)
        for vehicle in vehiclexylist:
            tsstr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(vehicle.timestamp))
            df.loc[-1] = [vehicle.timestamp, vehicle.vehicle_id, tsstr, vehicle.route_id, vehicle.lat, vehicle.lon]  # adding a row
            df.index = df.index + 1  # shifting index
            df = df.sort_values('Epoch Time (s)')  # sorting by index
        print '{0:3.3%}'.format(t/hinsec)+' done.'
        time.sleep(ps)

    now = datetime.datetime.now().strftime('%Y-%m-%d %H.%M.%S')

    csvpath = csvloc+"\\"+agency+" xy "+now+".csv"
    df.to_csv(csvpath, index=False)

    return csvpath

######
def route_dir_vehicle(vehiclelist,route,direction):
    """
    ????
    """
    vehicle_new=[]
    for a in vehiclelist:
        if getattr(a,'route_id')==route and getattr(a,'direction_id')==direction :
            vehicle_new.append(a)
    return vehicle_new

def get_prediction(agency,route_id,stop_id, direction):
    """
    return prediction of the specific stop
    """
    pre_list = []
    route = route_id_to_NB_tag.get(agency).get(route_id.lower())
    tag2 = stopdic_tag.get(agency).get(route)

    if int(stop_id) not in tag2:
        return pre_list
        #no prediction for this stop in Nextbus
    else:
        tag = tag2[int(stop_id)]

        url = nextbus_url + 'predictions&a=' + agency +'&r=' + route + '&s=' + tag

        try:
            pre_dict = u.get_json_from_url(url)
        except:
            print("JSON IS NOT READING, PLEASE CHECK")
            print
            return pre_list
            pass
            
        if 'direction' in pre_dict.get('predictions'):
            pre_list = pre_dict.get('predictions').get('direction').get('prediction')
            if isinstance(pre_list,dict):
                pre_list = [pre_list]

        return pre_list
    
#####get_seq
def distance(slat,slon,lat,lon):
    """
    calculate distance from two coordianate points
    :param lat of point1, longtitue of point1, lat of point2, longtitude of point2
    :return distance (km)
    """ 
    lat1 = radians(slat)
    lon1 = radians(slon)
    lat2 = radians(lat)
    lon2 = radians(lon)
    dlon = lon2-lon1
    dlat = lat2-lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    distance = R *  2 * atan2(sqrt(a), sqrt(1 - a))
    return distance

def get_seq_dict(agency):
    """
    get dictionary whose key as agency and trip id, value is a list of stop id order by sequence as the stop_times.txt
    :param agency:a str NextBus identifier of a transit agency
    :return dictionary of key as route, value as stop_id ordered by seq
    """
    stopseq = get_gtfs_df(webloc, agency, 'stop_times')
    dict_seq = {}
    for trip in set(stopseq['trip_id']):
        dict_seq[trip] = stopseq.loc[stopseq['trip_id']==trip,'stop_id'].tolist()
    return dict_seq

def current_stop_sequence2(agency, trip_id, lat, lon):
    """
    input agency and trip info and current location of the vehicle
    output the sequence of the closest stop to the vehicle
    """
    dic_of_stop_loc = stopdic_loc[agency]
    trip_id = int(trip_id)
    list_id_seq = get_seq_dict(agency).get(trip_id)
    #print("####listseq")
    #print(trip_id)
    #print(list_id_seq)
    #print("####listseq")
    return get_closest_stop(dic_of_stop_loc, list_id_seq, lat, lon)

def get_closest_stop(dic_stop, list_seq, lat, lon):
    """
    :param dic_Stop: dictionary of stop whose key is the stop id, value is the lat,lon of the stop
    :param list_seq: list of stop id ordered by sequence of the stop
    :param lat,lon: current location of the vehicle
    :return temp_index: the sequence of the closest stop, str(list_seq[temp_index]): stop id of the closest stop,
     not_near_stop: if the vehicle is too far away from the stop
    """
    temp = 100
    temp_index = -1
    not_near_stop = 0
    for i in range(len(list_seq)):
        if int(list_seq[i]) in dic_stop:
            s_lat,s_lon = dic_stop[int(list_seq[i])][0], dic_stop[int(list_seq[i])][1]
            dis = distance(s_lat,s_lon,lat,lon)
            if dis < temp:
                temp = dis
                temp_index = i
    if temp > 1:
        not_near_stop = 1
        
    closest_stop=temp_index
    
    s_lat,s_lon = dic_stop[int(list_seq[temp_index])][0], dic_stop[int(list_seq[temp_index])][1]
    lat0,lon0 = dic_stop[int(list_seq[0])][0], dic_stop[int(list_seq[0])][1]
    dis0_s = distance(s_lat,s_lon,lat0,lon0)
    disv_s = distance(lat,lon,lat0,lon0)
    if disv_s > dis0_s and temp_index < len(list_seq):
        next_stop=temp_index+1
    else:
        next_stop=temp_index
    return closest_stop,str(list_seq[closest_stop]),not_near_stop
    #return closest_stop,str(list_seq[closest_stop]),not_near_stop,str(list_seq[next_stop])

##get start time, feels better
def new_way_get_start_time(agency, trip_id, tftup, pre_lat, pre_lon, lat, lon, pre_start_time):
    """
    :param agency:a str NextBus identifier of a transit agency
    :param trip_id: trip id as in file trips.txt
    :param tftup: trip information
    :param pre_lat, prelon, lat,lon: previous location of vehicle and current location of vehicle
    :param pre_start_time: start time of the vehicle from last iteration
    :return start_time, new=1/0, when new==1, start time gets renew
    """
    new=0
    if diff_time(pre_start_time) < 10:
        return pre_start_time, new
    else:
        
        list_seq = get_seq_dict(agency).get(int(trip_id))
        station0 = int(list_seq[0])
        dic_stop = stopdic_loc[agency]
        stop0_lat,stop0_lon = dic_stop[station0][0], dic_stop[station0][1]
        dist_pre = distance(stop0_lat, stop0_lon, pre_lat, pre_lon)
        dist_now = distance(stop0_lat, stop0_lon, lat, lon)
        if 0.05 < dist_now - dist_pre and 1 > dist_now > 0.1:
            start_time = get_start_time5(tftup)
            new = 1
            return start_time,new
        else:
            return pre_start_time,new


def get_in_feed(fm,trip_id,tftup,vehicle,agency,r,p,idx,stop_id):
    myentity = fm.entity.add()
    myentity.id = vehicle.vehicle_id+'_'+vehicle.route_id+'_'+trip_id
    
    myentity.trip_update.timestamp = int(time.time())
    myentity.trip_update.trip.trip_id=trip_id
    myentity.trip_update.trip.route_id=vehicle.route_id
    myentity.trip_update.trip.direction_id=vehicle.direction_id
    myentity.trip_update.trip.start_time=start_time(agency,r,p.get('dirTag'),p.get('block'),tftup)
    myentity.trip_update.trip.start_date=str(get_service_day())
    #myentity.trip_update.trip.schedule_relation
    myentity.trip_update.vehicle.id=myentity.trip_update.vehicle.label=vehicle.vehicle_id
    #myentity.trip_update.stop_time_update
    stu = myentity.trip_update.stop_time_update.add()
    stu.stop_sequence=idx
    stu.stop_id=str(stop_id)
    stu.arrival.time=int(int(p.get('epochTime'))/1000)
    #stu.schedule_relationship=
    print(myentity.id,stu.stop_sequence,myentity.trip_update.trip.start_time)
    st=myentity.id+' '+str(stu.stop_sequence)+' '+myentity.trip_update.trip.start_time
    logger.info(st)

                                                                     
def start_time(a,r,dirTag,block,tftup):                       
    idx=datetime.datetime.today().weekday()
    SC = serviceClass.get(a)[idx]
    #print(a,r,SC,dirTag,block)
    start_time_list_per_block = schedule.get(a).get(r).get(SC).get(dirTag).get(block)
    start_time = binary_search(start_time_list_per_block)
    start_time = get_start_time_block(start_time,tftup) 
    return start_time
        
def binary_search(time_list):
    start=0
    last=len(time_list)
    found=False 
    now=datetime.datetime.now().time()
    while start<last and not found:
        mid=(start+last)//2

        if now<datetime.datetime.strptime(time_list[mid],"%H:%M:%S").time():
            last=mid
            if last-start==1:
                found=True
        else:
            start=mid
            if last-start==1:
                found=True
    return (time_list[start])

      
def get_start_time_block(start_time, tftup):
    """
    get the start time for the vehicle just set off from stop 0, which is the start time of current trip
    :param tftup: current trip info
    :return estimate start time in format hh:mm:ss
    """
    delta = u.string_to_dt(start_time) - u.string_to_dt(tftup.start_time)
    num_of_interval = int(delta.total_seconds() // tftup.headway_secs)
    start_time_trip = u.string_to_dt(tftup.start_time) + datetime.timedelta(0, num_of_interval * tftup.headway_secs)
    dt = datetime.time(hour = int(start_time_trip.hour),\
                       minute = int(start_time_trip.minute),
                       second = int(start_time_trip.second)).isoformat()
    return dt
       
    #http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=dc-circulator&r=blue&s=1stmary_n
#----------------------------------------------------------------------------------------------------------------------------#

def main():
    print 'Run something else.'

if __name__ == '__main__':
    main()