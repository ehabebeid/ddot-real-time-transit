import ddot_realtime as rt, utils as u
import pandas as pd, time, datetime
inittime = time.time()
webloc = 'C:\\Users\\eebeid\\Downloads'
archivename = 'wmata'

trips = rt.get_gtfs_df(webloc, archivename, 'trips')
routes = rt.get_gtfs_df(webloc, archivename, 'routes')
stop_times = rt.get_gtfs_df(webloc, archivename, 'stop_times')

routedict = {}
for _, row in routes.iterrows():
    routeid = row['route_id']
    rn = row['route_short_name']
    if rn in ['S2', 'S4', 'S9']:
        routedict[rn] = routeid
        
print routedict

df = pd.DataFrame()
for route in routedict:
    ntrips = 0
    print 'Route', route
    for _, row in trips.iterrows():
        tripid = int(row['trip_id'])
        #print '    Trip', tripid
        if row['route_id'] in routedict.values() and row['trip_headsign']=="SILVER SPRING STATION" and ntrips < 3:
            for _, row in stop_times.iterrows():
                thistripid = row['trip_id']
                if 16 < u.string_to_dt(row['departure_time']).hour < 18 and tripid == thistripid:
                    df = df.append(row,ignore_index=True)
                    ntrips += 1
    df.to_csv(webloc+'\stop_times_16thSt'+str(route)+'.csv')

print 'Time elapsed: '+'{:07.4f}'.format(time.time()-inittime)+' seconds. Terminated at '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"."

