#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        realtime gtfs for circulator from source bishop peak
# Author:      huixin rao
# Created:     2018/01/31
# -------------------------------------------------------------------------------

from __future__ import division
import os, sys, time, datetime
from trip_today import get_day_trips
from bishop_trip_update import recording
import ConfigParser, logging
from logging import FileHandler

scriptloc = sys.path[0]+'\\RT Feeds'
agency = 'dc-circulator'
webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

todaytripsdict ={}
previous={}

####logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
path = os.getcwd()
#logging handler
fh = FileHandler(path+'/temp/day_trip.log')
formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
######

def read_ini():
    """
    read command from .ini file
    if there's command need to stop the file
    :return true or false, true means the program keep running
    """
    config = ConfigParser.ConfigParser()
    config.read(path + "\initest.ini")
    if config.get('bishop peak','trip_update') == 'stop':
        user = config.get('user','user')
        logger.info("the system is stopped by " + user + ' at ' + datetime.datetime.now().strftime("%I:%M %p on %B %d, %Y"))
        return False
    else:
        return True


def main():
    print scriptloc
    run_trip_update(30,minutes=0,) # if want to keep running, minutes=0, if certain time length, minutes= time length

def run_trip_update(period, minutes):          # writes both pos and alerts  # subsititute for ru_sa
    i = s = 0     
    
    con = True if not minutes else False
    while s < minutes*60 or con:
        
        global todaytripsdict
        global previous
        
        duration = 0
        print 'Iteration', str(i+1)+';', s, 'seconds since start.'
        inittime = time.time()
        
        if not todaytripsdict:
            todaytripsdict = get_day_trips(agency)
            print("run trips dictionary")
            logger.info(datetime.date.today().strftime('%Y%m%d') + ': ' + ', '.join(map(str, todaytripsdict.keys())))
            
        elif datetime.datetime.now().hour==5 and datetime.datetime.now().minute>58:#renew tripdict at five everyday
            todaytripsdict = get_day_trips(agency)
            logger.info(datetime.date.today().strftime('%Y%m%d') + ': ' + ', '.join(map(str, todaytripsdict.keys())))
            
            previous={}
           
        if todaytripsdict is not None: 
            
            try: 
                previous = recording(todaytripsdict,previous)
            except IOError:
                print("IOError")
                pass
        
        duration += (time.time() - inittime)
        print '        Time elapsed: '+'{0:3.3f}'.format(duration)+' seconds. Terminated at '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"."
        print
        i+=1

        #stop when stop command is input in .ini file
        if s%300 == 0:
            con = read_ini()
            
        if duration<period:
            # script should sleep enough to reach period, not sleeping period blankly.
            sleeptime = period-duration
            s += int(sleeptime + duration)       # increment should be equal to period.
            time.sleep(sleeptime)
            
if __name__ == "__main__":
    main()