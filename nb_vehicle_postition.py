#!/usr/bin/env python2.7.3
# -------------------------------------------------------------------------------    
# Name:        vehicle position for nextbus as data source
# Author:      lucy rao
# Created:     2018/04/24
# -------------------------------------------------------------------------------

from __future__ import division
import sys, time, shutil, os
import google.transit.gtfs_realtime_pb2 as g  # @UnresolvedImport
from nb_ddot_realtime_functions import get_nextbus_agency_vl
import logging

scriptloc = sys.path[0]+'\\RT Feeds'
ext = '.pb'#'.txt'#

agencies = ['dc-circulator','dc-streetcar']

webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

tripsdict={}

logger = logging.getLogger()

def vehicle_position(agency):
    """
    getting vehicle position, prepared for vehicle position feed,
    not being used for now
    """
    filename = agency+'-'+'vehiclepositions'+ext
    
    vehiclelist = get_nextbus_agency_vl(agency)#vehicle_id, route_id, direction_id, dirtag, lat, lon, timestamp, bearing, speed, odometer
    fm,count = write_to_pb(vehiclelist)

    s1= '    Wrote', count, 'vehicle location updated with complete AVL info of', agency.replace('-', ' ')+'.'
    logger.info(s1)
    f = open(scriptloc+'\\'+filename, "wb")
    f.write(fm.SerializeToString())
    f.close()
    newpath = webloc+filename
    shutil.copyfile(os.path.join(scriptloc, filename), newpath) 

def write_to_pb(vehiclelist):
    fm = g.FeedMessage()
    
    ## Feed Header
    fm.header.gtfs_realtime_version = '2.0'
    fm.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number #enum: full dataset
    fm.header.timestamp = int(time.time())
    count=0
    for ve in vehiclelist:
        if not ve.vehicle_id:
            continue
        myentity = fm.entity.add()
        myentity.id = ve.vehicle_id

        if ve.vehicle_id:myentity.vehicle.vehicle.id = myentity.vehicle.vehicle.label = ve.vehicle_id
        if ve.lat: myentity.vehicle.position.latitude = ve.lat
        if ve.lon: myentity.vehicle.position.longitude = ve.lon
        if ve.speed: myentity.vehicle.position.speed = ve.speed
        if ve.bearing: myentity.vehicle.position.bearing = ve.bearing
        if ve.odometer: myentity.vehicle.position.odometer = ve.odometer
        myentity.vehicle.timestamp = ve.timestamp
        count+=1
        '''
        myentity.vehicle.trip.trip_id = myentity.trip_update.trip.trip_id
        myentity.vehicle.trip.route_id = myentity.trip_update.trip.route_id
        myentity.vehicle.trip.direction_id = myentity.trip_update.trip.direction_id
        myentity.vehicle.trip.start_time = myentity.trip_update.trip.start_time
        myentity.vehicle.trip.start_date = myentity.trip_update.trip.start_date
        '''
    return fm,count

'''  
def main():
    print scriptloc
    run_vehicle_position(30,minutes=0) # if want to keep running, minutes=0, if certain time length, minutes= time length

def run_vehicle_position(period, minutes):          # writes both pos and alerts  # subsititute for ru_sa
    i = s = 0     
    
    con = True if not minutes else False
    while s < minutes*60 or con:

        #global tripsdict
        duration = 0
        print 'Iteration', str(i+1)+';', s, 'seconds since start.'
        inittime = time.time()
        
        for agency in agencies:
            try: 
                vehicle_position(agency)
            except IOError:
                print("IOError")
                time.sleep(30)
            
        duration += (time.time() - inittime)
        print '        Time elapsed: ' + '{0:3.3f}'.format(duration) + ' seconds. Terminated at ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "."
        print
        i += 1
        if duration<period:
            # script should sleep enough to reach period, not sleeping period blankly.
            sleeptime = period - duration
            s += int(sleeptime + duration)       # increment should be equal to period.
            time.sleep(sleeptime)
        # implied else is: in the horrifying case that duration is more than period, iterate immediately. Shame.

if __name__ == "__main__":
    main()

'''