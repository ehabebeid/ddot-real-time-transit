import pandas as pd
import os

NB_tag_to_route_id = {
    "yellow": "Yellow",
    "green": "Green",
    "blue": "Blue",
    "rosslyn":"Turquoise",
    "potomac": "Orange",
    "mall": "Red",
    "h_route": "Red"
    }

dir_id_dict = {
    "dc-circulator": 
        {
        'blues': 0,
        'bluen': 1,
        'turquoisee': 0,
        'turquoisew': 1,
        'yelloww': 0,
        'yellowe': 1,
        'greenn': 1,
        'greens': 0,
        'orangew': 0,
        'orangee': 1,
        'redw': 0,
        'rede': 1
            },
    "dc-streetcar":
        {
        'redw': 1,
        'rede': 0
            }
    }

def get_stop_dict(agency):
    """
    read the NB_stop2.csv return dictionary where the route and direction are the key
    and the value is the dataframe of all the next bus stop on that direction
    :param agency: a str identifying the transit agency. Assumes a agency.zip GTFS archive.
    :returns dictionary of stop, key as route, value as dataframe of stops 
    """
    print("run stop dictionary")
    path = os.getcwd()
    
    if agency == 'dc-circulator':
        stopcsv=pd.read_csv(path+'/NB_stop2.csv')
    if agency == 'dc-streetcar':
        stopcsv=pd.read_csv(path+'/NB_stop4.csv')

    stopdic_loc={}
    stopdic_tag={}
    
    for i in range(len(stopcsv)):
        stopid=int(stopcsv.loc[i,'gtfs_id'])
        route=stopcsv.loc[i,'route']
        latlon=stopcsv.loc[i,['s_lat','s_lon']]
        stopdic_loc[stopid] = latlon
        if route not in stopdic_tag:
            stopdic_tag[route]={}
            stopdic_tag[route][stopid] = stopcsv.loc[i,'tag']
        else:
            stopdic_tag[route][stopid] = stopcsv.loc[i,'tag']
           
    return stopdic_loc,stopdic_tag