#!/usr/bin/env python2.7.3
import gspread, time
import utils as u
from oauth2client.service_account import ServiceAccountCredentials
from collections import namedtuple
import os
# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json

# Named tuple
ServiceAlert = namedtuple('ServiceAlert', 'agency_id start end cause effect stoplist routelist url header_en desc_en header_es desc_es')

path= os.getcwd()
def get_credentials():
    """Gets valid user credentials from script location."""
    scope = ['https://spreadsheets.google.com/feeds']
    return ServiceAccountCredentials.from_json_keyfile_name(path+'/ServiceAlerts-738fa0daf7d0.json', scope)

def get_active_service_alerts():
    """
    Read the spredsheet which stores the form responses
    for DDOT Circulator/Streetcar Service Alerts, and return
    the service alerts currently active or scheduled in the 
    future as list(ServiceAlert namedtuple).
    """
    creds = get_credentials()
    gc = gspread.authorize(creds)
    sh1 = gc.open_by_key('1Sm0gNshlyZfxe5NBnFEOyIoKu9vGkFgOkYzkkSRRCHA').sheet1
    #https://docs.google.com/spreadsheets/d/1Sm0gNshlyZfxe5NBnFEOyIoKu9vGkFgOkYzkkSRRCHA/edit#gid=684913790
    lod = sh1.get_all_records() # a list of dicts

    rl = []
    for row in lod:
        now = int(time.time())
        start = u.get_unix_time(u.get_date(row.get('Start Date and Time')), 'int')
        end = u.get_unix_time(u.get_date(row.get('End Date and Time')), 'int')

        if row.get('Timestamp') and now <= end and start < end:
        #if True: 
            agency = row.get('Agency')
            cause = row.get('Cause').replace(" ", "_").upper()
            effect = row.get('Effect').replace(" ", "_").upper()
            
            # stop list
            stops = row.get(agency+' Stops IDs')
            stops = stops.split(",") if stops else None
            # route list  
            routes = row.get(agency+' Routes')
            routes = routes.split(",") if routes else None

            url = row.get('URL')
            header_en = row.get('Header (English)')
            desc_en = row.get('Description (English)')
            header_es = row.get('Header (Spanish)')
            desc_es = row.get('Description (Spanish)')
            agency = agency.replace(" ","-").lower()  # Converts 'DC Streetcar' to 'dc-streetcar', the identifier
            rl.append(ServiceAlert(agency, start, end, cause, effect, stops, routes, url, header_en, desc_en, header_es, desc_es))
    print("read alerts")
    return rl

if __name__ == '__main__':
    print get_active_service_alerts()
