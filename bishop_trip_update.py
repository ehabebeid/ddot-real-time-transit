#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        use bishop api as input for trip update to google
# Author:      huixin rao
# Created:     2018/02/12
# -------------------------------------------------------------------------------

import os, sys, time, shutil
import google.transit.gtfs_realtime_pb2 as g
#from get_peak2 import get_vehicle, get_gtfsid, get_trip_info, get_eta
from bishop_ddot_realtime import  get_vehicle, get_gtfsid, get_trip_info, get_eta, get_seq_dict, distance
from utils import get_service_day
import logging
from logging.handlers import TimedRotatingFileHandler
import pandas as pd
#from ddot_realtime import get_seq_dict,distance#,get_nextbus_agency_vl
from collections import namedtuple

scriptloc = sys.path[0]+'\\RT Feeds'
ext = '.txt'#'.pb'#

agency = 'dc-circulator'
webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

routes_dict= {11318: "Yellow",#u'GT-US',
              11319: "Green",#u'WP-AM',
              11320: "Blue",#u'US-NY',
              11321: "Turquoise",#u'RS-DP',
              11322: "Orange",#u'PS',
              11323: "Red"}#"u'NM'}

VehicleInfo = namedtuple('VehicleInfo', 'vehicle_id, route_id, direction_id, \
                                        trip_id, bishop_trip, start_time, start_date\
                                        lat, lon, \
                                        nextstop, seq, eta, timestamp')

####logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
path = os.getcwd()
#logging handler
fh = TimedRotatingFileHandler(path+'/temp/ve_recording.log', when = 'midnight',interval = 1)
formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
######

df = pd.read_csv("new_stop.csv")[['route','direction','gtfs_id','stopID']]
seq_dict = get_seq_dict(agency)
returnlist={}
tuplelist={}

def recording(today_trip,previous):
    vehicle_list = get_vehicle()
    count=0
    
    count_eta=0
    '''
    vehicle_dict = vehicle_position(agency)
    
    for key,value in vehicle_dict.iteritems():
        print key,value.lat, value.lon, value.route_id, 'nextbux'
    
    '''
    start_date = str(get_service_day())
    for ve in vehicle_list:
        
        if ve.tripID < 100000 or ve.vehicle_id==1130 or ve.vehicle_id==1145:
            st = ', '.join(map(str, list(ve)))
            logger.info("tripID is 0, "+st)
            continue
        
        route = routes_dict[ve.route_id]
        
        if get_gtfsid(ve.nextStopID):
            stop_ID = get_gtfsid(ve.nextStopID)
        else:
            st = ', '.join(map(str, list(ve)))
            logger.info("next stop ID not in gtfs list, "+st)
            continue

        eta = get_eta(ve.route_id,ve.nextStopID)
        if eta: 
            if  abs(time.time()- eta)>3600:
                logger.info("eta is not accurate")
                count_eta +=1
                continue
            eta2= time.strftime('%H:%M:%S', time.localtime(int(eta)))
        else:
            st = ', '.join(map(str, list(ve)))
            logger.info("no eta, "+st)
            continue
        
        if ve_to_garage_distance(ve.lat,ve.lon):
            st = ', '.join(map(str, list(ve)))
            logger.info("vehicle in garage, "+st)
            continue

        trip_list=[]
        for trip_id,tup in today_trip.iteritems():
            if tup.route_id == route:
                trip_list.append(trip_id)

        if int(stop_ID) in seq_dict[trip_list[0]] and int(stop_ID) in seq_dict[trip_list[1]]:
            #trip_temp=str(trip_list[0])+" "+str(trip_list[1])
            #seq = str(seq_dict[trip_list[0]].index(int(stop_ID))) + " " + str(seq_dict[trip_list[1]].index(int(stop_ID)))
            if ve.vehicle_id in previous.keys():
                if ve.tripID == previous[ve.vehicle_id].bishop_trip:
                    
                    start_time2 = previous[ve.vehicle_id].start_time
                    trip_id_final = previous[ve.vehicle_id].trip_id
                    direction = today_trip[int(trip_id_final)].direction_id
                    seq = len(seq_dict[int(trip_id_final)])
                    #returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
                    
                    returnlist[ve.vehicle_id] = VehicleInfo(ve.vehicle_id, route, direction, \
                                        trip_id_final, ve.tripID, start_time2, start_date,\
                                        ve.lat, ve.lon, \
                                        stop_ID, seq, eta, ve.timestamp)
                else:
                    if previous[ve.vehicle_id].seq!= 0:
                        seq = 0
                        for trip in trip_list:
                            if seq_dict[trip][0]==int(stop_ID):
                                trip_id_final = trip
                                direction = today_trip[int(trip_id_final)].direction_id
                        start_time,start_time2 = get_trip_info(ve.tripID,ve.nextStopID)
                        #returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
                        returnlist[ve.vehicle_id] = VehicleInfo(ve.vehicle_id, route, direction, \
                                        trip_id_final, ve.tripID, start_time2, start_date,\
                                        ve.lat, ve.lon, \
                                        stop_ID, seq, eta, ve.timestamp)
                    else:
                        returnlist[ve.vehicle_id]=previous[ve.vehicle_id] 
            else:
                trip_id_final=str(trip_list[0])+" "+str(trip_list[1])
                seq = str(seq_dict[trip_list[0]].index(int(stop_ID))) + " " + str(seq_dict[trip_list[1]].index(int(stop_ID)))
                start_time,start_time2 = get_trip_info(ve.tripID,ve.nextStopID)
                start_date = str(get_service_day())
            
        elif int(stop_ID) in seq_dict[trip_list[0]]:
            trip_id_final = str(trip_list[0])
            direction = today_trip[int(trip_id_final)].direction_id
            seq = get_seq(trip_id_final, stop_ID)
            if ve.vehicle_id in previous.keys():
                if trip_id_final == previous[ve.vehicle_id].trip_id:
                    start_time2 = previous[ve.vehicle_id].start_time
                else:
                    start_time,start_time2 = get_trip_info(ve.tripID,ve.nextStopID)
            else:
                start_time,start_time2 = get_trip_info(ve.tripID,ve.nextStopID)
            returnlist[ve.vehicle_id] = returnlist[ve.vehicle_id] = VehicleInfo(ve.vehicle_id, route, direction, \
                                        trip_id_final, ve.tripID, start_time2, start_date,\
                                        ve.lat, ve.lon, \
                                        stop_ID, seq, eta, ve.timestamp)

        
        elif int(stop_ID) in seq_dict[trip_list[1]]:
            trip_id_final=str(trip_list[1])
            direction = today_trip[int(trip_id_final)].direction_id
            seq = get_seq(trip_id_final, stop_ID)
            if ve.vehicle_id in previous.keys():
                if trip_id_final == previous[ve.vehicle_id].trip_id:
                    start_time2 = previous[ve.vehicle_id].start_time
                else:
                    start_time,start_time2 = get_trip_info(ve.tripID,ve.nextStopID)
            else:
                start_time,start_time2 = get_trip_info(ve.tripID,ve.nextStopID)
            returnlist[ve.vehicle_id] = returnlist[ve.vehicle_id] = VehicleInfo(ve.vehicle_id, route, direction, \
                                        trip_id_final, ve.tripID, start_time2, start_date,\
                                        ve.lat, ve.lon, \
                                        stop_ID, seq, eta, ve.timestamp)
        
        else:
            print ve
        '''   
        if ve.vehicle_id in previous.keys():
            if trip_id_final == previous[ve.vehicle_id][1] and len(trip_id_final)<=2:
                #same trip, 
            if len(trip_id_final)>2 and trip_id_final != previous[ve.vehicle_id][1]:
                #same trip
            if len(trip_id_final)<=2 and trip_id_final != previous[ve.vehicle_id][1]:
                if len(previous[ve.vehicle_id][1])<2
                #new trip
                if len(previous[ve.vehicle_id][1])>=2
                #same trip
            if len(trip_id_final)>2 and len(previous[ve.vehicle_id][1])>=2:
                if ve.tripID same:
                #same trip
                if ve.tripID different:
                # new trip

        if ve.vehicle_id in previous.keys():
            if len(trip_id_final)<=2 and len(previous[ve.vehicle_id][1])<=2 and trip_id_final != previous[ve.vehicle_id][1]:
                #new trip 9,10
                print 'situation 1'
                returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
            elif len(trip_id_final)>2 and len(previous[ve.vehicle_id][1])>2 and ve.tripID != previous[ve.vehicle_id][4]:
                #new trip 9 10,9 10, different ve.trip
                print 'situation 2'
                for trip in trip_list:
                    if int(stop_ID) == seq_dict[trip][0]:
                        trip_id_final = trip
                        seq = 0
                        returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
            else:
                #same trip 
                if len(trip_id_final) <= 2 < len(previous[ve.vehicle_id][1]):
                    # 9 10,10
                    print 'situation 3'
                    #trip_id_final = trip_id_final
                    returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
                else:
                    #9,9 or 9,9 10 or 9 10, 9 10 same ve.trip
                    trip_id_final = previous[ve.vehicle_id][1]
                    print 'check1', trip_id_final, previous[ve.vehicle_id][1]
                    if isinstance(trip_id_final,int):
                        seq = get_seq(trip_id_final, stop_ID)
                        print 'situation 4'
                        start_time2 = previous[ve.vehicle_id][-1]
                        returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
                    else:
                        seq = previous[ve.vehicle_id][3]
                        print 'situation 5'
                        trip_id_final = previous[ve.vehicle_id][1]
                        print 'check2', trip_id_final, previous[ve.vehicle_id][1]
                        start_time2 = previous[ve.vehicle_id][-1]
                        returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
                        #print returnlist[ve.vehicle_id]
     
            if previous[ve.vehicle_id][1]!=trip_id_final:
                print(previous[ve.vehicle_id][1],trip_id_final)
                if ve.tripID == int(previous[ve.vehicle_id][-2]):
                    print("terminal")
                    trip_id_final=str(previous[ve.vehicle_id][1])
                    direction = today_trip[int(trip_id_final)].direction_id
                    seq= get_seq(trip_id_final, stop_ID)
                    start_time2 = previous[ve.vehicle_id][-1] 
                    returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
                    
                    #tuplelist[ve.vehicle_id] = VehicleInfo(ve.vehicle_id, route, direction, \
                    #                    trip_id, ve.tripID, start_time, start_date,\
                    #                    ve.lat, ve.lon, \
                    #                    stop_ID,  seq, eta, ve.timestamp) 
                    
                else:
                    print("new trip")
                    print (previous[ve.vehicle_id])
                    print(route,trip_id_final, stop_ID, seq, ve.tripID)
                    seq='0'
                    trip_list.remove(int(previous[ve.vehicle_id][1]))
                    trip_id_final = str(trip_list[0])
                    direction = today_trip[int(trip_id_final)].direction_id
                    returnlist[ve.vehicle_id]=[route,trip_id_final,stop_ID,seq,ve.tripID,start_time2]
                    print(returnlist[ve.vehicle_id])
                    #tuplelist[ve.vehicle_id] = VehicleInfo(ve.vehicle_id, route, direction, \
                    #                    trip_id, ve.tripID, start_time, start_date,\
                    #                    ve.lat, ve.lon, \
                    #                    stop_ID,  seq, eta, ve.timestamp) 
            '''       
        #st = str(ve.vehicle_id) + '{:>10}'.format(str(ve.tripID)) +'{:>12}'.format(route) +\
           # '{:>8}'.format(trip_id_final)  +'{:>10}'.format(start_time2) + '{:>10}'.format(stop_ID) \
           # + '{:>6}'.format(seq) + '{:>12}'.format(eta2) + '{:>12}'.format(ve.lat) + '{:>12}'.format(ve.lon) 

        #logger.info(st)
        #print(st)
    
        count+=1
    write_protobuff(returnlist)
    return returnlist

def get_seq(trip_id_final, stop_ID):
    
    if int(stop_ID)==0:
        seq = len(seq_dict[int(trip_id_final)])
    else:
        seq = str(seq_dict[int(trip_id_final)].index(int(stop_ID)))
    return str(seq)

def write_protobuff(unique_list):
    """
    returnlist[ve.vehicle_id] = returnlist[ve.vehicle_id] = VehicleInfo(ve.vehicle_id, route, direction, \
                                        trip_id_final, ve.tripID, start_time2, start_date,\
                                        ve.lat, ve.lon, \
                                        stop_ID, seq, eta, ve.timestamp)
    """


    filename = 'dc-circulator-'+'tripupdates'+ext
    fm = g.FeedMessage()
    fm.header.gtfs_realtime_version = '2.0'
    fm.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number
    
    trip_add={}
    count=0
    for id, veInfo in unique_list.iteritems():
    
        myentity = fm.entity.add()
        myentity.id = id + "_" + str(veInfo.trip_id )
        
        myentity.trip_update.timestamp =  veInfo.timestamp
        myentity.trip_update.vehicle.id = myentity.trip_update.vehicle.label = veInfo.vehicle_id
        myentity.trip_update.trip.route_id = veInfo.route_id
        myentity.trip_update.trip.direction_id = veInfo.direction_id
        myentity.trip_update.trip.trip_id = str(veInfo.trip_id) 
        myentity.trip_update.trip.start_time = veInfo.start_time
        
        if veInfo.trip_id not in trip_add.keys():
            trip_add[veInfo.trip_id]=[veInfo.start_time]
        elif veInfo.start_time in trip_add[veInfo.trip_id]:
            myentity.trip_update.trip.schedule_relationship = 1
        else:
            trip_add[veInfo.trip_id].append(veInfo.start_time)
            
        myentity.trip_update.trip.start_date = veInfo.start_date
        
        stu = myentity.trip_update.stop_time_update.add()
        stu.stop_id = veInfo.nextstop
        stu.stop_sequence = int(veInfo.seq)
        stu.arrival.time = veInfo.eta
        count+=1
        st = myentity.trip_update.vehicle.id+' '+ myentity.trip_update.trip.trip_id +' '+ stu.stop_id +' ' +str(stu.stop_sequence)+' '+myentity.trip_update.trip.start_time +\
        ' '+ str(stu.arrival.time)
        print(st)
    fm.header.timestamp = int(time.time())
    
    print '    Wrote', count, 'vehicles trip update updated with complete AVL info of dc circulator.'
    st='    Wrote '+str(count)+' vehicles trip update updated with complete AVL info of '+str(agency)
    logger.info(st)
    logger.info(' ')
    
    f = open(scriptloc+'\\'+filename, "wb")
    f.write(fm.SerializeToString())
    f.close()
    
    newpath = webloc+filename
    shutil.copyfile(os.path.join(scriptloc, filename), newpath)
    
  
def ve_to_garage_distance(lat,lon):
    slat = 38.914968
    slon = -76.978436
    if distance(slat,slon,lat,lon)<1:
        return True
    else: 
        return False        