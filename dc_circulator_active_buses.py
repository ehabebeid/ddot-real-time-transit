#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        Active Vehicles
# Author:      Ehab Ebeid
# Created:     2017/06/21
# -------------------------------------------------------------------------------

import time, datetime
from ddot_realtime import route_id_to_NB_tag, route_id_to_code, get_nextbus_route_vl
from pandas import DataFrame

def main():
    agency = 'dc-circulator'
    inittime = time.time()
    onroute = 0 # Number of vehicles assigned a route
    tc = 0 # Total number of vehicles
    csvloc = r"\\ddotfile02\DDOTSHARE\DDOTFSHARE\GIS\GTFS\Real-Time GTFS"
    columns = ['VehicleID','AssignedRoute']
    df = DataFrame(columns=columns)
    
    # Loop by route, by vehicle
    print 'ACTIVE BUSES --- THE DC CIRCULATOR'
    print
    for route in route_id_to_code.get(agency):
        cr = 0 # Number of vehicles on this route
        routedict = get_nextbus_route_vl(agency, route_id_to_NB_tag.get(agency).get(route))
        vehiclexylist = routedict['vehicle']
        routecode = route_id_to_code.get(agency).get(route)
        # A list of dictionaries. Each dict contains realtime position information about a vehicle on this route
        print 'Route '+routecode+':'
        for vehicle in vehiclexylist:
            if 'routeTag' in vehicle : onroute+=1
            tc+=1
            cr+=1
            
            df.loc[-1] = [vehicle['id'], routecode]  # adding a row
            df.index = df.index + 1  # shifting index
            df = df.sort_values('VehicleID')  # sorting by index
            
            print '    Vehicle # '+vehicle['id']
            
        print '    Route '+route_id_to_code[agency][route].strip()+' has '+str(cr)+" active vehicles."
        print
    print 'Retrieved '+str(tc)+' vehicles on all routes. '+str(onroute)+" of which are on route."
    
    now = datetime.datetime.now().strftime('%Y-%m-%d %H.%M.%S')
    csvpath = csvloc+r"\vehicleassignments "+now+".csv"
    
    df.to_csv(csvpath, index=False)
    
    termtime = time.time()
    print 'Time elapsed: '+'{0:3.3%}'.format(termtime-inittime)+' seconds. Terminated at '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"."
    print 'Results written to:'
    print '        ',csvpath
    print
    raw_input('Press ENTER to exit.')
    
if __name__ == '__main__':
    main()