#!/usr/bin/env python2.7.3
from __future__ import division
import os, sys, time, shutil, datetime
import google.transit.gtfs_realtime_pb2 as g  # @UnresolvedImport
from ddot_realtime import get_nextbus_agency_vl, get_day_trips, get_trip_id
from utils import get_service_day
from sheet_reader import get_active_service_alerts
#from test_tripupdate_vehicle import write_vehicle_pos_trip_update
#from newtest import write_vehicle_pos_trip_update
#from test_tripudate import write_vehicle_pos_trip_update

scriptloc = sys.path[0]+'\\RT Feeds'
ext = '.pb'#'.txt'#
agency = 'dc-circulator'
agencies = ['dc-circulator','dc-streetcar']
webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

previous={}
trip_start_time={}
tripsdict={}
tripsdict['dc-circulator']={}
tripsdict['dc-streetcar']={}
for agency in agencies:
    previous[agency]=0
def main():
    print scriptloc
    run_pos_alerts(30,minutes=0,) # if want to keep running, minutes=0, if certain time length, minutes= time length

def run_pos_alerts(period, minutes):          # writes both pos and alerts  # subsititute for ru_sa
    i = s = 0
    
    con = True if not minutes else False
    while s < minutes*60 or con:

        global previous
        global tripsdict
        duration = 0
        print 'Iteration', str(i+1)+';', s, 'seconds since start.'
        inittime = time.time()
        if s%(120) == 0:           #grab alerts run every 2 minutes
            for agency in agencies:
                write_alerts(agency)
            #print '        Time elapsed: '+'{0:3.3f}'.format(inittime - time.time())+' seconds. Terminated at '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"."
        '''
        try:
            for agency in agencies:
                #write_vehicle_pos(agency)
                if not tripsdict[agency]:
                    tripsdict[agency] = get_day_trips(agency, get_service_day())
                    print("run trips dictionary")
                elif datetime.datetime.now().hour==5:#renew tripdict at five everyday
                    tripsdict[agency] = get_day_trips(agency, get_service_day())
                    print("renew everyday")
                    
                previous=write_vehicle_pos_trip_update(agency,previous,tripsdict[agency])
                #print(agency,":",previous[agency])
                
        except IOError:
            print("IOError")
            time.sleep(30)
            pass
        
            #if s%300==0:
        for key,value in previous.items():
            print(key, value.vehicle.trip.trip_id, value.vehicle.current_stop_sequence, value.vehicle.trip.start_time)
            
        #print(previous)
        '''
        duration += (time.time() - inittime)
        print '        Time elapsed: '+'{0:3.3f}'.format(duration)+' seconds. Terminated at '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"."
        print
        i+=1
        if duration<period:
            # script should sleep enough to reach period, not sleeping period blankly.
            sleeptime = period-duration
            s+= int(sleeptime + duration)       # increment should be equal to period.
            time.sleep(sleeptime)
        # implied else is: in the horrifying case that duration is more than period, iterate immediately. Shame.

##################################################################

def write_alerts(agency):
    """ 
    Using an input named tuple (alerts) in the format below,
    and for the given agency, write a service alert .pb file.
    
    :param agency: a str of the agency_id, i.e. 'dc-circulator' or 'dc-streetcar'
    :param alerts: a named tuple declared as:
                ServiceAlert = namedtuple('ServiceAlert', 'agency_id start end cause effect stoplist routelist url header_en desc_en header_es desc_es')
    """
    filename = agency+'-'+'servicealerts'+ext
    """ """
    alerts=get_active_service_alerts()
    # Feed Message
    fm = g.FeedMessage()
    ## Feed Header
    fm.header.gtfs_realtime_version = '2.0'
    fm.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number #enum: full dataset
    count = 0
    
    
    for alert in alerts:
        agency_id, start, end, cause, effect, stops, routes, url, header_en, desc_en, header_es, desc_es = alert 
        
        if agency_id == agency:
            if stops:
                for s in stops:# only one entity in each feed!               
                    count += 1
                    write_entity(count, fm, 's', s, agency_id, start, end, cause, effect, url, header_en, desc_en, header_es, desc_es)

            if routes:
                for r in routes:                 
                    count += 1
                    write_entity(count, fm, 'r', r, agency_id, start, end, cause, effect, url, header_en, desc_en, header_es, desc_es)

    fm.header.timestamp = int(time.time())

    f = open(scriptloc+'\\'+filename, "wb")
    f.write(fm.SerializeToString())
    f.close()
    
    #print 'Wrote .pb file to script location.'
    
    newpath = webloc+filename
    shutil.copyfile(os.path.join(scriptloc, filename), newpath)
    #print 'Copied .pb file to web location.'
    print '    Wrote ', count, 'current or future service alerts for the', agency.replace('-', ' ')+'.'

def write_entity(count,fm, entity_type,entity, agency_id, start, end, cause, effect, url, header_en, desc_en, header_es, desc_es):

    myentity = fm.entity.add()
    myentity.id = cause+'_'+entity+'_'+time.strftime('%Y%m%d.%H:%M:%S', time.localtime(start)) + '_' + time.strftime('%Y%m%d.%H:%M:%S', time.localtime(end))
    period = myentity.alert.active_period.add()
    period.start = start
    period.end = end
                    
    myentity.alert.cause = g._ALERT_CAUSE.values_by_name[cause].number
    myentity.alert.effect = g._ALERT_EFFECT.values_by_name[effect].number
    
    if entity_type == 'r':            
        r = entity.replace(" ","")
        ie = myentity.alert.informed_entity.add()
        ie.route_id = r 
    if entity_type == 's':
        s = entity.replace(" ","")
        ie = myentity.alert.informed_entity.add()
        ie.stop_id = s
        
        
    urlen = myentity.alert.url.translation.add()
    urlen.text = url
    urlen.language = 'en'
        
    headeren = myentity.alert.header_text.translation.add()
    headeren.text = header_en
    headeren.language = 'en'
        
    descen = myentity.alert.description_text.translation.add()
    descen.text = desc_en
    descen.language = 'en'
        
    if header_es:
        headeres = myentity.alert.header_text.translation.add()
        headeres.text = header_es
        headeres.language = 'es'
        
    if desc_es:
        desces = myentity.alert.description_text.translation.add()
        desces.text = desc_es
        desces.translation = 'es'   
           
            
if __name__ == "__main__":
    main()