#!/usr/bin/env python2.7.3
from __future__ import division
import os, sys, time, shutil, datetime
import google.transit.gtfs_realtime_pb2 as g  # @UnresolvedImport
from ddot_realtime import get_day_trips
#from utils import get_service_day
from sheet_reader import get_active_service_alerts
from test_tripupdate_vehicle import write_vehicle_pos_trip_update
import logging
from logging.handlers import TimedRotatingFileHandler
from alert_writer import write_alerts
from getstarttime import get_day_schedule_dict

scriptloc = sys.path[0]+'\\RT Feeds'
ext = '.txt'#'.pb'#
agency = 'dc-circulator'
agencies = ['dc-circulator','dc-streetcar']
webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

previous={}
trip_start_time={}
tripsdict={}
tripsdict['dc-circulator']={}
tripsdict['dc-streetcar']={}
for agency in agencies:
    previous[agency]=0
schedule_dict={}     
####logging test

logger=logging.getLogger('writer')
logger.setLevel(logging.INFO)
path = os.getcwd()
fh=TimedRotatingFileHandler(path+'/temp/day/test.log',
                                    when='D',interval=1)

formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)


def main():
    print scriptloc
    run_pos_alerts(30,minutes=0,) # if want to keep running, minutes=0, if certain time length, minutes= time length

def run_pos_alerts(period, minutes):          # writes both pos and alerts  # subsititute for ru_sa
    i = s = 0
    
    con = True if not minutes else False
    while s < minutes*60 or con:

        global previous
        
        global tripsdict
        duration = 0
        print 'Iteration', str(i+1)+';', s, 'seconds since start.'
        inittime = time.time()

        '''
        if s%(120) == 0:           #grab alerts run every 2 minutes
            for agency in agencies:
                write_alerts(agency)
            #print '        Time elapsed: '+'{0:3.3f}'.format(inittime - time.time())+' seconds. Terminated at '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"."
        '''
        try:
            for agency in agencies:
                #write_vehicle_pos(agency)
                if not tripsdict[agency]:
                    tripsdict[agency] = get_day_trips(agency)
                    print("run trips dictionary")
                    logger.info("run trips dictionary")
                    logger.info(tripsdict[agency])
                    schedule_dict[agency]=get_day_schedule_dict(agency)
                    
                elif datetime.datetime.now().hour==5 and datetime.datetime.now().minute>58:#renew tripdict at five everyday
                    tripsdict[agency] = get_day_trips(agency)
                    
                    schedule_dict[agency]=get_day_schedule_dict(agency)
                    
                    print("renew everyday")
                    logger.info("renew everyday")
                    logger.info(tripsdict[agency])
                    previous[agency]=0
                    
                if tripsdict[agency] is not None: 
                    if datetime.datetime.now().hour==6:  
                        logger.info("at 6: ")
                        logger.info(previous[agency])

                    previous[agency] = write_vehicle_pos_trip_update(agency,previous[agency],tripsdict[agency],schedule_dict[agency])

                else:
                    print (datetime.datetime.now(),tripsdict[agency])
            logger.info("##############")
            #logger.info(previous)
        except IOError:
            print("IOError")
            logging.error("IOError")
            time.sleep(30)
            pass
        
            #if s%300==0:
        #for key,value in previous.items():
        #    print(key, value.vehicle.trip.trip_id, value.vehicle.current_stop_sequence, value.vehicle.trip.start_time)
            
        #print(previous)
        duration += (time.time() - inittime)
        print '        Time elapsed: '+'{0:3.3f}'.format(duration)+' seconds. Terminated at '+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+"."
        print
        i+=1
        if duration<period:
            # script should sleep enough to reach period, not sleeping period blankly.
            sleeptime = period-duration
            s += int(sleeptime + duration)       # increment should be equal to period.
            time.sleep(sleeptime)
        # implied else is: in the horrifying case that duration is more than period, iterate immediately. Shame.

if __name__ == "__main__":
    main()