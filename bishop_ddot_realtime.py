
from __future__ import division

import utils as u
from math import sin, cos, sqrt, atan2, radians
from collections import namedtuple
import pandas as pd
from zipfile import ZipFile
import datetime


VehicleNew = namedtuple('VehicleData', 'vehicle_id, route_id, lat, lon, timestamp, minsLate, nextStopID, tripID')

agency='dc-circulator'
webloc = '\\\\ddotwebapp03\\CirculatorData\\gtfs'

apibase = "http://api.peaktransit.com/v5/index.php"
#?app_id=_DCC&key=96e6540989984739d5bb0b4bf86f2f9f&controller='
app_id="_DCC"
key="96e6540989984739d5bb0b4bf86f2f9f"
agencyID='&agencyID=45'


webloc_bishop = apibase + "?app_id=" + app_id + "&key=" + key + "&controller="
controller1,controller2="gtfs","gtfsrt"
R = 6373.0
df = pd.read_csv("new_stop.csv")[['route','direction','gtfs_id','stopID']]


def get_gtfs_df(webloc, archivename, filename):
    """
    Return a Pandas CSV DataFrame object of filename.txt,
    located within the .zip archive, with name
    archivename.zip located within webloc folder.
    :param webloc: str, location of GTFS .zip archive
    :param archivename: str, name of .zip archive, without '.zip'.
    :param filename: str, name of .txt file inside archive, without '.txt'. Must be one of the GTFS .txt files.
    :returns a pandas DateFrame object containing what's in the comma-separated txt/csv.
    """
    archive = ZipFile(webloc+"\\"+archivename+".zip")
    f = archive.open(filename+".txt")
    return pd.io.parsers.read_csv(f)  # @UndefinedVariable because PyDev can't read submodule

def get_trip_id(todaytripsdict, route_id, direction_id):
    """
    With a dict of the trips active today and through a series 
    of comparisons decide which trip the vehicle info belongs to.
    Returns trip_id matching GTFS file.
    
    :param todaytripsdict: a dict of today's trips for a given agency, in the format returned by get_day_trips(agency, today)
           This is used instead of generating it inside this so as to avoid generating for each vehicle.
    :param route_id: a str corresponding with the route_id field in GTFS/GTFS-RT feeds of this agency
    :param direction_id: an int corresponding with direction_id field in GTFS/GTFS-RT feeds of this agency
    :param datetime: experimental field, optional. Defaults to datetime obj now()
    :returns str trip_id, and corresponding tuple
    """
    for trip_id, tftup in todaytripsdict.iteritems():
        if route_id is not None and route_id == tftup.route_id:
            if direction_id is not None and direction_id == tftup.direction_id:
                start, end = u.string_to_dt(tftup.start_time), u.string_to_dt(tftup.end_time)
                if start <= datetime.datetime.now() <= end + datetime.timedelta(hours=1):
                    return str(trip_id), tftup


def get_seq_dict(agency):
    """
    get dictionary whose key as agency and trip id, value is a list of stop id order by sequence as the stop_times.txt
    :param agency:a str NextBus identifier of a transit agency
    :return dictionary of key as route, value as stop_id ordered by seq
    """
    stopseq = get_gtfs_df(webloc, agency, 'stop_times')
    dict_seq = {}
    for trip in set(stopseq['trip_id']):
        dict_seq[trip] = stopseq.loc[stopseq['trip_id']==trip,'stop_id'].tolist()
    return dict_seq

def distance(slat,slon,lat,lon):
    """
    calculate distance from two coordianate points
    :param lat of point1, longtitue of point1, lat of point2, longtitude of point2
    :return distance (km)
    """ 
    lat1 = radians(slat)
    lon1 = radians(slon)
    lat2 = radians(lat)
    lon2 = radians(lon)
    dlon = lon2-lon1
    dlat = lat2-lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    distance = R *  2 * atan2(sqrt(a), sqrt(1 - a))
    return distance

def get_vehicle_url():
    url_veh = webloc_bishop + controller2 + "&action=" + "vehiclePositions" + agencyID
    return  u.get_json_from_url(url_veh).get("vehicle")

def get_vehicle():
    """
    get vehicle info from controller gtfrt
    """
    veh_list = get_vehicle_url()
    ve_list=[]
    for i in veh_list:
        #if i.get('tripDescriptor').get('tripID') <100000:
        #    continue
        timestamp = i.get('timestamp')
        vehicle_id = i.get('vehicleDescriptor').get('label')
        route_id = i.get('tripDescriptor').get('routeID')
        lat,lon = i.get('position').get('latitude'), i.get('position').get('longitude')
        stopID = i.get('currentPassage').get('stopID')
        tripID = i.get('tripDescriptor').get('tripID')
        minsLate = 0
        ve_list.append(VehicleNew(vehicle_id, route_id, lat, lon, timestamp, minsLate, stopID, tripID))
    return ve_list

def get_gtfsid(nextStopID):
    """
    get regianl id from bishop peak stop id 
    :param nextStopID: next stop id in the bishop peak format as 6 digit number from 113120 to 114181
    :return string of regional id
    """
    if any(df.stopID == nextStopID):
        gtfs_id = df.loc[df["stopID"]==nextStopID]['gtfs_id'].item()
        return str(gtfs_id)
    else:
        print(nextStopID)
        return None

def get_trip_info(tripID,nextStopID,seqq=0):
    """
    get the start time of each trip from bishop peak
    :param tripID: trip id in bishop paek format in 6 digit 
    :param nextStopID: next stop id in the bishop peak format as 6 digit number from 113120 to 114181
    """
    if tripID!=0:
        url_trip = webloc_bishop+'gtfs'+'&action='+'stopTimes'+agencyID + "&tripID=" +str(tripID) 
        
        
        for i in range(3):
            try:
                stop_list = u.get_json_from_url(url_trip).get("stopTimes")
                #print("clear")
            except:
                print("error")
                print(url_trip)
                stop_list=[]
                continue
            else:
                break
        if stop_list:
            start_time = stop_list[0].get("departureTime")
            if int(start_time.split(':')[0]) >= 24:
                start_time = '0'+str(int(start_time.split(':')[0])-24)+':'+start_time.split(':')[1]+':'+start_time.split(':')[2]
            if seqq!=0:
                print(stop_list[0].get("stopID"), nextStopID, tripID)
            start_time2 = to_zero(start_time)
            return start_time,start_time2
        else:
            #in case the url cannot be reached
            return '0','0'

def to_zero(start_time):
    h, m, s = start_time.split(":") 
    minu = '{:0>2}'.format(str(int(m) // 10 * 10))
    start_time_to_0 = h + ":" + minu + ":" + s
    return start_time_to_0

def get_eta(routeID,stopID):
    """
    get the estimate arrival time json
    :return dictionary of eta, key as routeID,stopID, value as list of the estimate arrival time
    :output_example: eta in epochtime format
    """
    url_eta = webloc_bishop + "eta" + "&action=" + "list" + agencyID + "&routeID=" + str(routeID) + "&stopID=" + str(stopID)
    #print(url_eta)
    for i in range(3):
        try:
            eta_list = u.get_json_from_url(url_eta).get("stop")
            #print("clear")
        except:
            eta_list = []
            print("eta error", url_eta)
            continue
        else:
            break
    if eta_list:     
        return eta_list[0].get("ETA1") 
    else: 
        return eta_list

def main():
    print 'Run something else.'

if __name__ == '__main__':
    main()