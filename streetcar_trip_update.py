#!/usr/bin/env python2.7.3
from __future__ import division
import os, sys, time, shutil
import google.transit.gtfs_realtime_pb2 as g  # @UnresolvedImport
from ddot_realtime_clean import get_nextbus_agency_vl, current_stop_sequence2, \
        get_trip_id, get_seq_dict,start_time
import logging
from logging.handlers import TimedRotatingFileHandler
from stop_dict_generator import get_stop_dict
import utils as u
from utils import get_service_day

scriptloc = sys.path[0]+'\\RT Feeds'
ext = '.txt'#'.pb'#

webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"
agency='dc-streetcar'
####logging test
logger = logging.getLogger()
logger.setLevel(logging.INFO)
path = os.getcwd()
#logging handler
fh = TimedRotatingFileHandler(path+'/log/streetcar_logging/log_test.log', when = 'midnight',interval = 1)
fh.suffix = "%Y-%m-%d"
formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
######

route_id_to_NB_tag = { #Circulator route colours mapped onto NextBus Circulator route tags       
    "dc-streetcar":
        {
        "red": "h_route"
        }
    }

agencydict = {
    'dc-streetcar' : ['red']
    }

nextbus_url = 'http://webservices.nextbus.com/service/publicJSONFeed?command=' 

stopdic_loc = {}
stopdic_tag = {}

stopdic_loc[agency], stopdic_tag[agency] = get_stop_dict(agency)

def write_streetcar_trip_update(agency,tripsdict):
    #,schedule_dict,ve_block
    """
    TODO: document
    # fix the no direction problem
    # fix the sequence number mismatch problem
    
     
    :param agency:
    :param tripsdict:
    return dictionary of myentity, key as enetity id ,value as the whole entity
    """
    filename = agency+'-'+'tripupdates'+ext

    vehiclelist = get_nextbus_agency_vl(agency) #now it should be vehicle info tuple

    # Feed Message
    fm = g.FeedMessage()
    
    ## Feed Header
    fm.header.gtfs_realtime_version = '2.0'
    fm.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number #enum: full dataset
    count = 0

    #input sample:
    #VehiclePositionData(vehicle_id=u'1140', route_id='Yellow', direction_id=0, lat=38.899306, lon=-77.00453, timestamp=1503431200, bearing=303, speed=1.1111111111111112, odometer=None),
    if vehiclelist:
        for vehicle in vehiclelist: 
            #print "#",vehicle.vehicle_id
            if vehicle.route_id:
                r = route_id_to_NB_tag[agency][vehicle.route_id.lower()]
                #taglist = stopdic_tag.get(agency).get(r)
                if vehicle.direction_id is not None:
                    direction=vehicle.direction_id

                    if get_trip_id(tripsdict, vehicle.route_id, direction):
                        trip_id, tftup = get_trip_id(tripsdict, vehicle.route_id, direction)
                    else:
                        print "becuase of trip dict"
                        print tripsdict, vehicle.route_id, vehicle.direction_id
                        break
                                   
                    seq,current_stop_id,not_near_stop= current_stop_sequence2(agency, trip_id, vehicle.lat, vehicle.lon)
                        #print(vehicle,trip_id,seq,current_stop_id,not_near_stop)
                    if not_near_stop==0:
                            #print("route:",r, trip_id)
                        taglist = stopdic_tag.get(agency).get(r)
                            #
                        has=0
                        list_id_seq = get_seq_dict(agency).get(int(trip_id))
                        if int(current_stop_id) in list_id_seq:
                            idx = list_id_seq.index(int(current_stop_id))
                        else:
                            idx = 0   
                        count = get_predict(agency,has,idx,list_id_seq,taglist,r,vehicle,trip_id,tftup,fm,count,direction)
                        '''
                            while has==0 and idx<len(list_id_seq):
                                #print("idx",idx,len(list_id_seq))
                                stop_id = list_id_seq[idx]
                                #print(int(stop_id), idx, list_id_seq)
                                if int(stop_id) in taglist:
                                    tag = taglist.get(int(stop_id))            
                                    url = nextbus_url + 'predictions&a=' + agency +'&r=' + r + '&s=' + tag
                                    try:
                                        pre_dict = u.get_json_from_url(url).get('predictions')
                                    except:
                                        print "this stop no prediction"
                                        pre_dict=[]
                                        pass
                                    if 'direction' in pre_dict:
                                        p = pre_dict.get('direction').get('prediction')
                                        if isinstance(p,dict):
                                            if int(p.get('vehicle'))==int(vehicle.vehicle_id):
                                                #print("stopid:",stop_id)
                                                #print("route_id",r)
                                                #print("trip_id:",trip_id)
                                                #print p
                                                #print(start_time(agency,r,p.get('dirTag'),p.get('block'),tftup))
                                                get_in_feed(fm,trip_id,tftup,vehicle,agency,r,p,idx,stop_id)
                                                
                                                count+=1
                                                has=1
                                                break
                                            else:
                                                    idx+=1
                                        else:
                                            
                                            for p in [0:2]:
                                                #print p.get('vehicle')
                                                if int(p.get('vehicle'))==int(vehicle.vehicle_id):
                                                    #print("stopid:",stop_id)
                                                    #print("route_id",r)
                                                    #print("trip_id:",trip_id)
                                                    #print p
                                                    #print(start_time(agency,r,p.get('dirTag'),p.get('block'),tftup))
                                                    get_in_feed(fm,trip_id,tftup,vehicle,agency,r,p,idx,stop_id)
                                                    
                                                    count+=1
                                                    has=1
                                                    break
                                                else:
                                                    idx+=1
                                    else:
                                        print "no prediction"
                                else:
                                    idx+=1
                        '''       
                    else:
                        print "not_near_stop"
                        logger.info("not_near_stop")
                else:
                    print "!no direction",vehicle
                    logger.info("!no direction")
                        
                        #print(get_dir(agency,vehicle))
            else:
                print "no route id"
                logger.info("no route id" )                   
            
        
    ## Feed Header; updated here as opposed to above to be more recent
    fm.header.timestamp = int(time.time())

    print '    Wrote', count, 'vehicles pos and trip update updated with complete AVL info of', agency.replace('-', ' ')+'.'
    st='    Wrote '+str(count)+' vehicles pos and trip update updated with complete AVL info of '+str(agency)
    logger.info(st)
    logger.info(' ')
    f = open(scriptloc+'\\'+filename, "wb")
    f.write(fm.SerializeToString())
    f.close()
    #print 'Wrote .pb file to script location.'
    
    newpath = webloc+filename
    shutil.copyfile(os.path.join(scriptloc, filename), newpath)
    #print 'Copied .pb file to web location.'
'''
def get_closest_stop_next_stop(agency,list_id_seq,lat,lon,):
    temp=100
    for i in list_id_seq:
        if i in stopdic_loc[agency]:
            s_lat,s_lon = stopdic_loc[agency][i][0],stopdic_loc['agency][i][1]
            dis=distance(s_lat,s_lon,lat,lon)
            #print(dis)
            if dis<temp:
                temp=dis
                stop_id=i
            else:
                print stop_id
                print (list_id_seq.index(stop_id)+1)#sequence
                break
    seq=list_id_seq.index(stop_id)+1
    lat0,lon0 = stopdic_loc['dc-circulator'][list_id_seq[0]][0], stopdic_loc['dc-circulator'][list_id_seq[0]][1]
    dis0_s = distance(s_lat,s_lon,lat0,lon0)
    s_lat,s_lon = stopdic_loc['dc-circulator'][stop_id][0],stopdic_loc['dc-circulator'][stop_id][1]
    disv_s = distance(lat,lon,lat0,lon0)
    if dis0_s<disv_s and list_id_seq.index(stop_id)+1<len(list_id_seq):
        seq_next=seq+1

    else:
        seq_next=seq

    return seq,list_id_seq[seq-1],seq_next
    
tag = stopdic_tag[agency][route][list_id_seq[seq_next-1]]
p = get_predict_from_url(agency,'rosslyn',tag)
print p

def get_closest_stop(stop_dict_loc, stop_dic_tag, list_seq, lat, lon):
    """
    :paran list_seq: list of stops of a specific trip 
    """
    temp = 100
    temp_index = -1
    not_near_stop = 0
    
    for i in range(len(list_seq)):
        if int(list_seq[i]) in dic_stop:
            s_lat,s_lon = dic_stop[int(list_seq[i])][0], dic_stop[int(list_seq[i])][1]
            dis = distance(s_lat,s_lon,lat,lon)
            if dis < temp:
                temp = dis
                temp_index = i
    if temp > 1:
        not_near_stop = 1
        
    closest_stop=temp_index
    
    s_lat,s_lon = dic_stop[int(list_seq[temp_index])][0], dic_stop[int(list_seq[temp_index])][1]
    lat0,lon0 = dic_stop[int(list_seq[0])][0], dic_stop[int(list_seq[0])][1]
    dis0_s = distance(s_lat,s_lon,lat0,lon0)
    disv_s = distance(lat,lon,lat0,lon0)
    if disv_s > dis0_s and temp_index < len(list_seq):
        next_stop=temp_index+1
    else:
        next_stop=temp_index
    return closest_stop,str(list_seq[closest_stop]),not_near_stop
'''

def get_predict(agency,has,idx,list_id_seq,taglist,r,vehicle,trip_id,tftup,fm,count,direction):
    while has==0 and idx<len(list_id_seq):
        #print("idx",idx,len(list_id_seq))
        stop_id = list_id_seq[idx]
        #print(int(stop_id), idx, list_id_seq)
        if int(stop_id) in taglist:
            tag = taglist.get(int(stop_id))            
            p = get_predict_from_url(agency,r,tag)
            if p:
                if isinstance(p,dict):
                    if p.get('vehicle')==vehicle.vehicle_id:
                        get_in_feed(fm,trip_id,tftup,vehicle,agency,r,p,idx,stop_id,direction)          
                        #print(      entity.trip_update.stop_time_update.stop_sequence)
                        count+=1
                        has=1
                        break
                    else:
                        idx+=1
                else:                    
                    for p in p[0:2]:
                        if p.get('vehicle')==vehicle.vehicle_id:
                            get_in_feed(fm,trip_id,tftup,vehicle,agency,r,p,idx,stop_id,direction)                  
                            #print(      entity.trip_update.stop_time_update.stop_sequence)
                            count+=1
                            has=1
                            break
                        else:
                            idx+=1
            else:
                idx+=1
                
        else:
            idx+=1  
    #logger.info(entity.id,entity.trip_update.trip.trip_id,entity.trip_update.stop_time_update.stop_sequence,entity.trip_update.trip.start_time)              
    
    return count
    
def get_predict_from_url(agency,r,tag):
    url = nextbus_url + 'predictions&a=' + agency +'&r=' + r + '&s=' + tag
    try:
        pre_dict = u.get_json_from_url(url).get('predictions')
    except:
        print "this stop no prediction"
        logger.info("this stop no prediction")
        pass
        pre_dict=[]
    if 'direction' in pre_dict:
        p = pre_dict.get('direction').get('prediction')
    else:
        p=[]
        print "no prediction"
        logger.info("no prediction")
    return p

def get_in_feed(fm,trip_id,tftup,vehicle,agency,r,p,idx,stop_id,direction):
    myentity = fm.entity.add()
    myentity.id = vehicle.vehicle_id+'_'+vehicle.route_id+'_'+trip_id
    
    myentity.trip_update.timestamp = int(time.time())
    myentity.trip_update.trip.trip_id=trip_id
    myentity.trip_update.trip.route_id=vehicle.route_id
    myentity.trip_update.trip.direction_id=direction
    myentity.trip_update.trip.start_time=start_time(agency,r,p.get('dirTag'),p.get('block'),tftup)
    myentity.trip_update.trip.start_date=str(get_service_day())
    #myentity.trip_update.trip.schedule_relation
    myentity.trip_update.vehicle.id=myentity.trip_update.vehicle.label=vehicle.vehicle_id
    #myentity.trip_update.stop_time_update
    stu = myentity.trip_update.stop_time_update.add()
    stu.stop_sequence=idx
    stu.stop_id=str(stop_id)
    stu.arrival.time=int(int(p.get('epochTime'))/1000)
    #stu.schedule_relationship=
    print(myentity.id,stu.stop_sequence,myentity.trip_update.trip.start_time)
    st=myentity.id+' '+str(stu.stop_sequence)+' '+myentity.trip_update.trip.start_time
    logger.info(st)

dir_id_dict = {
    "dc-circulator": 
        {
        'blues': 0,
        'bluen': 1,
        'turquoisee': 0,
        'turquoisew': 1,
        'yelloww': 0,
        'yellowe': 1,
        'greenn': 1,
        'greens': 0,
        'orangew': 0,
        'orangee': 1,
        'redw': 0,
        'rede': 1
            },
    "dc-streetcar":
        {
        'redw': 1,
        'rede': 0
            }
    }

def get_dir(agency,vehicle):
    
    rou = route_id_to_NB_tag[agency][vehicle.route_id.lower()]
    tag_list = stopdic_tag[agency][rou]
    for _,v in tag_list.iteritems():
        p=get_predict_from_url(agency,rou,v)
        if p:
            if isinstance(p,dict):
                if p.get('vehicle')==vehicle.vehicle_id:
                    dirtag=p.get('dirTag')
                    direction_id = dir_id_dict[agency][vehicle.route_id.lower()+dirtag[0]]
                    #trip_id, tftup = get_trip_id(tripsdict[agency], vehicle.route_id, vehicle.direction_id)
                    return direction_id
                    break
                else:
                    continue
            else:
                for p in p:
                    if p.get('vehicle')==vehicle.vehicle_id:
                        dirtag=p.get('dirTag')
                        print(vehicle.route_id.lower()+dirtag[0])
                        
                        route_tag=vehicle.route_id.lower()+dirtag[0]
                        
                        direction_id = dir_id_dict[agency][route_tag]
                        #trip_id, tftup = get_trip_id(tripsdict[agency], vehicle.route_id, vehicle.direction_id)
                        return direction_id
                        break
                    else:
                        continue
    