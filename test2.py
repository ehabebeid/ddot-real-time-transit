#!/usr/bin/env python2.7.3

# -------------------------------------------------------------------------------    
# Name:        DDOT Real-Time Transit
# Author:      Huixin Rao
# Created:     2017/09/12
# -------------------------------------------------------------------------------
#to fin the vechicle position 
#TODO: myentity.vehicle.current_stop_sequence, int
            #question: i know the trip id, how can i know the current stop to figure out the seq? dictionary for seq done

import numpy as np
import pandas as pd
from math import sin, cos, sqrt, atan2, radians
from ddot_realtime import get_gtfs_df

webloc = '\\\\ddotwebapp03\\CirculatorData\\gtfs'
agency='dc-circulator'
#change the NB route and dir into the one that matche the gtfs 
#to this instead of change the input because the dictionary is built one time but the input is sth that keeps coming

route_id_to_NB_tag = { #Circulator route colours mapped onto NextBus Circulator route tags
    "dc-circulator":
        {
        "yellow": "yellow",
        "green": "green",
        "blue": "blue",
        "turquoise":"rosslyn",
        "orange": "potomac",
        "red": "mall"
        },
                    
    "dc-streetcar":
        {
        "red": "h_route"
        }
    }

NB_tag_to_route_id = {
    "yellow": "Yellow",
    "green": "Green",
    "blue": "Blue",
    "rosslyn":"Turquoise",
    "potomac": "Orange",
    "mall": "Red",
    "h_route": "Red"
    }

dir_id_dict = {
    "dc-circulator": 
        {
        'blues': 0,
        'bluen': 1,
        'turquoisee': 0,
        'turquoisew': 1,
        'yelloww': 0,
        'yellowe': 1,
        'greenn': 1,
        'greens': 0,
        'orangew': 0,
        'orangee': 1,
        'redw': 0,
        'rede': 1
            },
    "dc-streetcar":
        {
        'redw': 1,
        'rede': 0
            }
    }

def get_stop_dict(agency):
    """
    read the NB_stop2.csv return dictionary where the route and direction are the key
    and the value is the dataframe of all the next bus stop on that direction
    :param agency: a str identifying the transit agency. Assumes a agency.zip GTFS archive.
    :returns
    """
    #TODO: the dictionary keys are not corect yet!!!!! still mall and nsew, fixed to the red and 01 version!!!!!!
    stopcsv=pd.read_csv('P:/DDOTFSHARE/GIS/GTFS/Real-Time GTFS/NB_stop2.csv')
    stopdic={}#should add a key level for agency
    
    for route in set(stopcsv['route']):
        route2=NB_tag_to_route_id[route]
        stopdic[route2]={}
        for direc in set(stopcsv[stopcsv['route']==route]['direction']):
            direction_id=dir_id_dict.get(agency).get((route2+direc).lower())
            #stopdic[route][direc]=stopcsv.loc[stopcsv['route']==route].loc[stopcsv['direction']==direc].iloc[:,[0,1,2,7]]
            stopdic[route2][direction_id]=stopcsv.loc[stopcsv['route']==route].loc[stopcsv['direction']==direc]
        #dftemp=stopcsv.loc[stopcsv['route']=='blue'].loc[stopcsv['direction']=='s']
    
    return stopdic
'''
def current_stop_sequence(agency, route, trip_id, direction, lat, lon):
    #this is a list containing all the lonlat of the stops in the route+dir
    #route, trip_id, dir, can get from trips_dict
    """
    input agency and trip info and current location of the vehicle
    output the sequence of the closest stop to the vehicle
    """
    df_of_stop_loc=get_stop_dict(agency).get(route).get(direction)
    
    stop_gtfsid=get_stop_gtfsid(df_of_stop_loc, lat, lon)
    #print("agency, trip_id, stop_gtfsid:",agency, trip_id, stop_gtfsid)
    seq=get_seq(agency, trip_id, stop_gtfsid)
    
    return seq
'''
def get_stop_gtfsid(df,lat, lon):
    """
    input the dataframe contains stop info of certain route and direction,
    longtitue and latitude of the vehicle
    find the stop that's the most close to the vehicle and return the gtfs_id of the stop
    """
    dic_dist={}    
    for _,row in df.iterrows():
        slat=row['s_lat']
        slon=row['s_lon']
        dist=distance(slat,slon,lat,lon)
        dic_dist[dist]=row['gtfs_id']
    distmin=min(dic_dist.keys())
    return dic_dist[distmin]

def distance(slat,slon,lat,lon):
    """
    calculate distance from two coordianate points
    input: lat of point1, longtitue of point1, lat of point2, longtitude of point2
    output: distance
    """
    R = 6373.0
    lat1=radians(slat)
    lon1=radians(slon)
    lat2=radians(lat)
    lon2=radians(lon)
    dlon=lon2-lon1
    dlat=lat2-lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    return distance
'''
def get_seq(agency, tripid,stopid):
    """
    find the stop seq according to the trip id and stop id 
    """
    temp=get_seq_dict(agency)[tripid]# one problem is what if the stop id does not exist in this trip
    return temp.loc[temp['stop_id']==stopid,'stop_sequence'].values[0]
'''
def get_seq_dict(agency):
    """
    read stop_times.txt return dictionary using trip id as key 
    and value is a dataframe containing stopid and stop sequence
    """
    dict_seq={}
    stopseq=get_gtfs_df(webloc, agency, 'stop_times')
    
    for trip in set(stopseq['trip_id']):
        dict_seq[trip]=stopseq.loc[stopseq['trip_id']==trip,['stop_id','stop_sequence']]
        #stopcsv.loc[stopcsv['route']==route].loc[stopcsv['direction']==direc]
    return dict_seq


'''
#vehiclelist = get_nextbus_agency_vl(agency)
#vehicle=vehiclelist[0]
#myentity.vehicle.current_stop_sequence=
from collections import namedtuple
VehiclePos = namedtuple('VehiclePositionData', 'vehicle_id, route_id, direction_id, lat, lon, timestamp, bearing, speed, odometer')

vehicle=VehiclePos(vehicle_id=u'2118', route_id='Yellow', direction_id=0, lat=38.9028909, lon=-77.062773, timestamp=1505738793, bearing=354, speed=3.611111111111111, odometer=None)
trip_id=23
print(current_stop_sequence(agency, vehicle.route_id, trip_id, vehicle.direction_id, vehicle.lat, vehicle.lon))





input dict_seq[tripid]
#trip_id, tftup = get_trip_id(tripsdict, vehicle.route_id, vehicle.direction_id)
df_of_stop_loc=get_stop_dict(agency).get(route).get(direction)


temp=-1
for _,row in seq_dict.iterrows():
    stop_id=row[0]
    stop_seq=row[1]
    stop_lat_lon=df_of_stop_loc.loc[df_of_stop_loc["gtfs_id"]==stop_id,['s_lat','s_lon']]
    disnow=func_distance(stopid):# input stopid get lat lon of the stop, 
    if temp>=0 and disnow>=temp:
        return stop_seq-1
    else:
        temp=disnow
'''        
def get_seq(df_stop, df_seq, lat, lon):
    temp=-1

    for _,row in df_seq.iterrows():
        stop_id=row[0]
        
        stop_seq=row[1]
        stop_lat_lon=df_stop.loc[df_stop["gtfs_id"]==stop_id,['s_lat','s_lon']]
        s_lat=stop_lat_lon.values[0][0]
        s_lon=stop_lat_lon.values[0][1]
        disnow=distance(s_lat,s_lon,lat,lon)
        if temp>=0 and disnow>=temp:
            seq=stop_seq-1
            break
        else:
            temp=disnow
    return seq

def current_stop_sequence(agency, route, trip_id, direction, lat, lon):
    #this is a list containing all the lonlat of the stops in the route+dir
    #route, trip_id, dir, can get from trips_dict
    """
    input agency and trip info and current location of the vehicle
    output the sequence of the closest stop to the vehicle
    """
    df_of_stop_loc = stop_dic[agency].get(route).get(direction)
    df_id_seq = get_seq_dict(agency).get(trip_id)
    
    seq=get_seq(df_of_stop_loc, df_id_seq, lat, lon)
    
    return seq


stop_dic={}
stop_dic['dc-circulator']=get_stop_dict('dc-circulator')

from collections import namedtuple
VehiclePos = namedtuple('VehiclePositionData', 'vehicle_id, route_id, direction_id, lat, lon, timestamp, bearing, speed, odometer')

vehicle=VehiclePos(vehicle_id=u'2118', route_id='Yellow', direction_id=0, lat=38.9028909, lon=-77.062773, timestamp=1505738793, bearing=354, speed=3.611111111111111, odometer=None)
trip_id=23
print(agency, vehicle.route_id, trip_id, vehicle.direction_id, vehicle.lat, vehicle.lon)


df_of_stop_loc = stop_dic[agency].get(vehicle.route_id).get(vehicle.direction_id)
df_id_seq = get_seq_dict(agency).get(trip_id)
    
seq=get_seq(df_of_stop_loc, df_id_seq, vehicle.lat, vehicle.lon)

print(seq)