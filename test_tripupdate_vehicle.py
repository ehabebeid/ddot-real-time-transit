#!/usr/bin/env python2.7.3
from __future__ import division
import os, sys, time, shutil, datetime
import google.transit.gtfs_realtime_pb2 as g  # @UnresolvedImport
from ddot_realtime import get_nextbus_agency_vl, current_stop_sequence2, \
        get_trip_id, get_prediction, estimate_start_time, diff_time2,\
        new_way_get_start_time, get_start_time5
from utils import string_to_dt,get_service_day
import logging
from logging.handlers import TimedRotatingFileHandler
from getstarttime import find_block

scriptloc = sys.path[0]+'\\RT Feeds'
ext = '.txt'#'.pb'#

agencies = ['dc-circulator','dc-streetcar']
webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

####logging test
logger = logging.getLogger()
logger.setLevel(logging.INFO)
path = os.getcwd()
#logging handler
fh = TimedRotatingFileHandler(path+'/temp/test.log', when = 'h',interval = 1)
formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
######

def write_vehicle_pos_trip_update(agency,previous,tripsdict,schedule_dict):
    """
    TODO: document
    # To make script run faster, today's trips are generated here (once per VP function call, or even once per day). 
    # The resulting dict is then passed as an argument to getTripsID.
    # This results in time savings of 3 to 4 seconds per call to this function.
    # tripsdict is a dict of today's trips and their frequency, to better match a vehicle's tripID
    # done with this todo
     
    :param agency:
    :param tripsdict:
    return dictionary of myentity, key as enetity id ,value as the whole entity
    """
    filename = agency+'-'+'vehiclepositions'+ext
    filename_tu = agency+'-'+'tripupdates'+ext

    vehiclelist = get_nextbus_agency_vl(agency) #now it should be vehicle info tuple

    # Feed Message
    fm = g.FeedMessage()
    
    ## Feed Header
    fm.header.gtfs_realtime_version = '2.0'
    fm.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number #enum: full dataset
    count = 0
    
    # Feed Message
    fm_tu = g.FeedMessage()
    
    ## Feed Header
    fm_tu.header.gtfs_realtime_version = '2.0'
    fm_tu.header.incrementality = g._FEEDHEADER_INCREMENTALITY.values_by_name['FULL_DATASET'].number #enum: full dataset
    returnlist={}
    #input sample:
    #VehiclePositionData(vehicle_id=u'1140', route_id='Yellow', direction_id=0, lat=38.899306, lon=-77.00453, timestamp=1503431200, bearing=303, speed=1.1111111111111112, odometer=None),
    if vehiclelist:
        for vehicle in vehiclelist: 
                
            if (vehicle.vehicle_id and vehicle.route_id and vehicle.direction_id is not None):
                if get_trip_id(tripsdict, vehicle.route_id, vehicle.direction_id):
                    trip_id, tftup = get_trip_id(tripsdict, vehicle.route_id, vehicle.direction_id)

                else:
                    logger.info("because of tripdict")
                    print("because of tripdict")
                    break
                
                    #trip_id, tftup = get_trip_id(tripsdict, vehicle.route_id, vehicle.direction_id)
                if string_to_dt(tftup.end_time) < datetime.datetime.now():# if the trip already ends, dont write into the feed
                    logger.info("because of time")
                    print("becuase of time")
                    break
                
                else:
                    seq,current_stop_id,not_near_stop, next_stop_id = current_stop_sequence2(agency,  trip_id, vehicle.lat, vehicle.lon)
                    prediction_list = get_prediction(agency,vehicle.route_id,next_stop_id, vehicle.direction_id)

                if not_near_stop == 0 and len(prediction_list) > 0:
                
                    myentity = fm.entity.add()
                    myentity.id = vehicle.vehicle_id+'_'+vehicle.route_id
                
                    ### Vehicle Position
                    if vehicle.timestamp: myentity.vehicle.timestamp = vehicle.timestamp
                
                        #TODO: myentity.vehicle.congestion_level, enum
                        #where to get the info of congestion? 
                        #if vehicle.congestion_level:myentity.vehicle.congestion_level=g._VehiclePosition_CongestionLevel.value[vehicle.congestion_level].number#g._ALERT_CAUSE.values_by_name[cause].number
                        #TODO: myentity.vehicle.occupancy_status, enum, exp field
                        #"The optional "congestion_level" and "occupancy_status data" felds for message VehiclePosition are not currently provided by NextBus data feed."
                        #from https://nextbus.cubic.com/Portals/nextbus/Addendum%20to%20GTFS_RT.pdf
                
                	    #TODO: myentity.vehicle.current_status, enum
                        #All other optional felds are provided and vehicle's 'current_status' is set to IN_TRANSIT_TO value. Other VehicleStopStatus values are not currently used for vehicle's current status.
                
                        #TODO: myentity.vehicle.stop_id, string. I think this is only if vehicle is dwelling, but not sure
                
                	    #### Trip Descriptor
                
                    entity_tu = fm_tu.entity.add()
                    entity_tu.id = vehicle.vehicle_id+'_'+vehicle.route_id+'_'+trip_id
                    if vehicle.timestamp: entity_tu.trip_update.timestamp = vehicle.timestamp
                    if trip_id: 
                        myentity.vehicle.trip.trip_id = entity_tu.trip_update.trip.trip_id = trip_id
                    if vehicle.route_id: 
                        myentity.vehicle.trip.route_id = entity_tu.trip_update.trip.route_id = vehicle.route_id
                    
                    if vehicle.direction_id is not None: 
                        myentity.vehicle.trip.direction_id = entity_tu.trip_update.trip.direction_id = vehicle.direction_id
     
                    # myentity.vehicle.trip.start_date, string
                    myentity.vehicle.trip.schedule_relationship = 0 #enum: scheduled
    
                    #### Vehicle Descriptor
                    myentity.vehicle.vehicle.label = myentity.vehicle.vehicle.id = vehicle.vehicle_id
                    entity_tu.trip_update.vehicle.id=entity_tu.trip_update.vehicle.label = vehicle.vehicle_id

                    count+=1
    
                    #### Position
                    if vehicle.lat: myentity.vehicle.position.latitude  = vehicle.lat
                    if vehicle.lon: myentity.vehicle.position.longitude = vehicle.lon
                    if vehicle.speed: myentity.vehicle.position.speed = vehicle.speed
                    if vehicle.bearing: myentity.vehicle.position.bearing = vehicle.bearing
                    if vehicle.odometer: myentity.vehicle.position.odometer = vehicle.odometer
                
                    myentity.vehicle.current_stop_sequence = seq
                    myentity.vehicle.stop_id = current_stop_id
    
                    #stop time update:
                    stu=entity_tu.trip_update.stop_time_update.add()
                    stu.stop_sequence = seq
                    stu.stop_id = current_stop_id
                    stu.arrival.time = int(int(prediction_list[0].get('epochTime'))/1000)
                    
                    #find the block
                    print(vehicle.vehicle_id,seq, current_stop_id,next_stop_id, vehicle.lat, vehicle.lon)
                    print(prediction_list)
                    for i in prediction_list:
                        print(i.get('vehicle'))
                        if int(i.get('vehicle'))==int(vehicle.vehicle_id):
                            vehicle_block = i.get('block')
                            vehicle_dir = i.get('dirTag')
                            
                            start_time_fromblock = find_block(vehicle.route_id,schedule_dict,vehicle_block,vehicle_dir,tftup)
                            print(vehicle.vehicle_id,vehicle.route_id,vehicle_block,vehicle_dir, start_time_fromblock)   
                    
                    
                    #start time for circulator
                    if agency == 'dc-circulator':
                        if previous!=0 and myentity.id in previous:
                            if seq == 0:
                                start_time,new = new_way_get_start_time(agency, trip_id, tftup, previous[myentity.id].vehicle.position.latitude, previous[myentity.id].vehicle.position.longitude,myentity.vehicle.position.latitude,myentity.vehicle.position.longitude,previous[myentity.id].vehicle.trip.start_time)
                                if new == 1:
                                    print("cl Bon Voyage")
                                    print(datetime.datetime.now())
                                    myentity.vehicle.trip.start_time = start_time
                                    myentity.vehicle.trip.start_date = str(get_service_day())
                                    print(myentity.id,myentity.vehicle.trip.trip_id, myentity.vehicle.current_stop_sequence, myentity.vehicle.trip.start_date, myentity.vehicle.trip.start_time,"pre:",previous[myentity.id].vehicle.current_stop_sequence,previous[myentity.id].vehicle.trip.start_time)
                                        
                                else:
                                    myentity.vehicle.trip.start_time = previous[myentity.id].vehicle.trip.start_time
                                    myentity.vehicle.trip.start_date = previous[myentity.id].vehicle.trip.start_date
                                
                            else:
                                myentity.vehicle.trip.start_time = previous[myentity.id].vehicle.trip.start_time
                                myentity.vehicle.trip.start_date = previous[myentity.id].vehicle.trip.start_date
     
                        else:#first round
                            print(myentity.id,"cl estimate start time")
                            myentity.vehicle.trip.start_time = estimate_start_time(myentity.vehicle.current_stop_sequence,tftup)
                            myentity.vehicle.trip.start_date = str(get_service_day())
                            diff2 = diff_time2(myentity.vehicle.trip.start_time, tftup.start_time)
                            if diff2 < 0:
                                myentity.vehicle.trip.start_time = tftup.start_time 
                            print(myentity.vehicle.current_stop_sequence, myentity.vehicle.trip.start_date, myentity.vehicle.trip.start_time)
                        entity_tu.trip_update.trip.start_time = myentity.vehicle.trip.start_time
                        entity_tu.trip_update.trip.start_date = myentity.vehicle.trip.start_date
                    #start time for streetcar
                    if agency == 'dc-streetcar':
                        
                        if previous != 0 and myentity.id in previous:
                            if trip_id != previous[myentity.id].vehicle.trip.trip_id:
                                print("sc Bon Voyage")
                                print(datetime.datetime.now())
                                myentity.vehicle.trip.start_time = get_start_time5(tftup)
                                myentity.vehicle.trip.start_date = str(get_service_day())
                                print(myentity.id,myentity.vehicle.trip.trip_id, myentity.vehicle.current_stop_sequence, myentity.vehicle.trip.start_date, myentity.vehicle.trip.start_time,"pre:",previous[myentity.id].vehicle.trip.trip_id,previous[myentity.id].vehicle.current_stop_sequence,previous[myentity.id].vehicle.trip.start_time)
                                
                            else:
                                myentity.vehicle.trip.start_time = previous[myentity.id].vehicle.trip.start_time
                                myentity.vehicle.trip.start_date = previous[myentity.id].vehicle.trip.start_date
                        else:
                            print(myentity.id,"sc estimate start time")
                            myentity.vehicle.trip.start_time = estimate_start_time(myentity.vehicle.current_stop_sequence,tftup)
                            myentity.vehicle.trip.start_date = str(get_service_day())
                            diff2 = diff_time2(myentity.vehicle.trip.start_time, tftup.start_time)
                            if diff2 < 0:
                                myentity.vehicle.trip.start_time = tftup.start_time 
                                
                            print(myentity.vehicle.current_stop_sequence, myentity.vehicle.trip.start_date, myentity.vehicle.trip.start_time)
                        
                        entity_tu.trip_update.trip.start_time = myentity.vehicle.trip.start_time
                        entity_tu.trip_update.trip.start_date = myentity.vehicle.trip.start_date
                        
                    returnlist[myentity.id] = myentity
                    logst = str(entity_tu.id)+' '+str(entity_tu.trip_update.trip.trip_id)+' '+str(stu.stop_sequence)+' '+entity_tu.trip_update.trip.start_date+' '+str(entity_tu.trip_update.trip.start_time)
                    logger.info(logst)
                    
                    #print(entity_tu.id, entity_tu.trip_update.trip.trip_id, stu.stop_sequence, entity_tu.trip_update.trip.start_time)
                    #print(entity_tu)
                    #logfinal=str(vehicle,route, direction, lat,lon,stop seq)
                else:
                    logger.info("no tripdict")   
    else:
        print('no vehiclelist')
        logger.info('no vehiclelsit')
        
    ## Feed Header; updated here as opposed to above to be more recent
    fm.header.timestamp = int(time.time())
    fm_tu.header.timestamp = int(time.time())

    print '    Wrote', count, 'vehicles pos and trip update updated with complete AVL info of', agency.replace('-', ' ')+'.'
    st='    Wrote '+str(count)+' vehicles pos and trip update updated with complete AVL info of '+str(agency)
    logger.info(st)
    logger.info(' ')
    f = open(scriptloc+'\\'+filename, "wb")
    f.write(fm.SerializeToString())
    f.close()
    #print 'Wrote .pb file to script location.'
    
    newpath = webloc+filename
    shutil.copyfile(os.path.join(scriptloc, filename), newpath)
    #print 'Copied .pb file to web location.'
    
    fu = open(scriptloc+'\\'+filename_tu, "wb")
    fu.write(fm_tu.SerializeToString())
    fu.close()
    newpath = webloc+filename_tu
    shutil.copyfile(os.path.join(scriptloc, filename_tu), newpath)
    
    return returnlist


