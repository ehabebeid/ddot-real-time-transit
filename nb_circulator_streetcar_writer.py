#!/usr/bin/env python2.7.3
from __future__ import division
import sys, time, datetime, os
from trip_today import get_day_trips
from nb_circulator_streetcar_trip_update import circulator_trip_update
import ConfigParser, logging
from logging.handlers import TimedRotatingFileHandler

scriptloc = sys.path[0]+'\\RT Feeds'

agencies = ['dc-circulator','dc-streetcar']

webloc = "\\\\ddotwebapp03\\CirculatorData\\gtfs\\"

tripsdict={}

####logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
path = os.getcwd()
#logging handler
fh = TimedRotatingFileHandler(path+'/temp/multest2.log', when = 'midnight',interval = 1)
fh.suffix = "%Y-%m-%d"
formatter = logging.Formatter('%(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

def main():
    print scriptloc
    run_trip_update(30,minutes=0) # if want to keep running, minutes=0, if certain time length, minutes= time length

def read_ini():
    """
    read command from .ini file
    if there's command need to stop the file
    :return true or false, true means the program keep running
    """
    config = ConfigParser.ConfigParser()
    config.read(path + "\initest.ini")
    if config.get('run or stop','trip_update') == 'stop':
        user = config.get('user','user')
        logger.info("the system is stopped by " + user + ' at ' + datetime.datetime.now().strftime("%I:%M %p on %B %d, %Y"))
        return False
    else:
        return True
        
def run_trip_update(period, minutes):          # writes both pos and alerts  # subsititute for ru_sa
    i = s = 0     
    
    con = True if not minutes else False
    while s < minutes*60 or con:            
        global tripsdict
        duration = 0
        s1 =  'Iteration', str(i+1)+';', s, 'seconds since start.'
        print s1
        logger.info(s1)
        inittime = time.time()
        
        for agency in agencies:
            tripsdict[agency] = []
            if not tripsdict[agency]:
                tripsdict[agency] = get_day_trips(agency)
                        
            elif datetime.datetime.now().hour == 5 and datetime.datetime.now().minute > 58:#renew tripdict at five everyday
                tripsdict[agency] = get_day_trips(agency)
            
            if tripsdict[agency] is not None: 
                try: 
                    circulator_trip_update(tripsdict[agency],agency)
                    #vehicle_position(agency)
                except IOError:
                    print("IOError")
                    time.sleep(30)
            
        duration += (time.time() - inittime)
        print '        Time elapsed: ' + '{0:3.3f}'.format(duration) + ' seconds. Terminated at ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "."
        print
        logger.info('        Time elapsed: ' + '{0:3.3f}'.format(duration) + ' seconds. Terminated at ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ".")
        logger.info("")
        i += 1
        
        #stop when stop command is input in .ini file
        if s%300 == 0:
            con = read_ini()
        
        if duration<period:
            # script should sleep enough to reach period, not sleeping period blankly.
            sleeptime = period - duration
            s += int(sleeptime + duration)       # increment should be equal to period.
            time.sleep(sleeptime)
        # implied else is: in the horrifying case that duration is more than period, iterate immediately. Shame.

if __name__ == "__main__":
    main()